-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 09, 2018 at 04:19 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yii2advanced`
--

-- --------------------------------------------------------

--
-- Table structure for table `file_type`
--

CREATE TABLE `file_type` (
  `id` int(15) NOT NULL,
  `file` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `file_type`
--

INSERT INTO `file_type` (`id`, `file`) VALUES
(1, 'PDF'),
(2, 'Word'),
(3, 'PNG'),
(4, 'Excel'),
(5, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1530011517),
('m130524_201442_init', 1530011519);

-- --------------------------------------------------------

--
-- Table structure for table `sdlc_phase`
--

CREATE TABLE `sdlc_phase` (
  `ID` int(11) NOT NULL,
  `phase` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sdlc_phase`
--

INSERT INTO `sdlc_phase` (`ID`, `phase`) VALUES
(1, 'Code'),
(2, 'Review'),
(3, 'Recode'),
(4, 'Resolved');

-- --------------------------------------------------------

--
-- Table structure for table `task`
--

CREATE TABLE `task` (
  `task_id` int(11) NOT NULL,
  `case_id` int(11) NOT NULL,
  `practice_id` int(11) DEFAULT NULL,
  `case_manager` varchar(50) DEFAULT NULL,
  `request_received_on` date DEFAULT NULL,
  `no_of_pages` int(11) DEFAULT NULL,
  `revised_pages` int(11) DEFAULT NULL,
  `assigned_to` varchar(50) DEFAULT NULL,
  `note` varchar(50) DEFAULT NULL,
  `assigned_on` date DEFAULT NULL,
  `reviewer` varchar(50) DEFAULT NULL,
  `review_assigned_on` date DEFAULT NULL,
  `review_done` varchar(10) DEFAULT NULL,
  `review_done_on` date DEFAULT NULL,
  `send_for_review` varchar(50) DEFAULT NULL,
  `recode_sent_to` varchar(50) DEFAULT NULL,
  `recode_sent_on` date DEFAULT NULL,
  `send_for_recode` varchar(10) DEFAULT NULL,
  `close` varchar(10) DEFAULT NULL,
  `closed_date` date DEFAULT NULL,
  `unplanned` varchar(10) DEFAULT NULL,
  `sdlc_phase` varchar(10) DEFAULT NULL,
  `type_of_request` varchar(25) DEFAULT NULL,
  `file_type` varchar(10) DEFAULT NULL,
  `estimated_time` decimal(10,0) DEFAULT NULL,
  `development_time` decimal(10,0) DEFAULT NULL,
  `review_time` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `task`
--

INSERT INTO `task` (`task_id`, `case_id`, `practice_id`, `case_manager`, `request_received_on`, `no_of_pages`, `revised_pages`, `assigned_to`, `note`, `assigned_on`, `reviewer`, `review_assigned_on`, `review_done`, `review_done_on`, `send_for_review`, `recode_sent_to`, `recode_sent_on`, `send_for_recode`, `close`, `closed_date`, `unplanned`, `sdlc_phase`, `type_of_request`, `file_type`, `estimated_time`, `development_time`, `review_time`) VALUES
(2, 7548125, 525, 'test', '2018-04-23', 109, 5, 'evonnelo    ', 'testnote', '2018-05-08', 'nikitaja    ', '2018-05-08', 'yes', '2018-05-08', 'no', 'atulga    ', '0000-00-00', 'yes', 'yes', '2018-05-08', 'yes', 'Resolved', 'Collector Code', 'Word', '23', '21', '3.00'),
(11, 111, 1, 'a', '2018-05-04', 2, NULL, 'atulga    ', '', NULL, '', NULL, 'prompt', NULL, 'prompt', '', NULL, 'prompt', 'yes', NULL, 'yes', 'Recode', 'Clinical Code', '', NULL, NULL, NULL),
(12, 222, 2, '', '2018-04-30', 3, NULL, '', '', NULL, 'test', NULL, 'prompt', NULL, 'prompt', '', NULL, 'prompt', 'no', NULL, 'yes', 'Code', 'Collector Code', '', NULL, NULL, NULL),
(13, 333, 3, '', '2018-05-18', 4, NULL, '', '', NULL, '', NULL, 'prompt', NULL, 'prompt', '', NULL, 'prompt', 'yes', NULL, 'no', 'Code', 'Clinical Tweak', '', NULL, NULL, NULL),
(14, 444, 4, '', '2018-05-28', 75, NULL, '', '', NULL, '', NULL, 'prompt', NULL, 'prompt', '', NULL, 'prompt', 'yes', NULL, 'yes', 'Recode', 'Clinical Tweak', '', NULL, NULL, NULL),
(16, 555, 5, '', '2018-04-21', 6, NULL, '', '', NULL, '', NULL, 'prompt', NULL, 'prompt', '', NULL, 'prompt', 'no', NULL, 'no', 'Resolved', 'Clinical Code', '', NULL, NULL, NULL),
(17, 666, 6, '', '2018-05-21', 7, NULL, '', '', NULL, '', NULL, 'prompt', NULL, 'prompt', '', NULL, 'prompt', 'yes', NULL, 'no', 'Review', 'Clinical Tweak', '', NULL, NULL, NULL),
(18, 777, 7, '', '2018-05-10', 8, NULL, '', '', NULL, '', NULL, 'prompt', NULL, 'prompt', '', NULL, 'prompt', 'yes', NULL, 'no', 'Review', 'Clinical Code', '', NULL, NULL, NULL),
(19, 888, 8, '', '2018-05-25', 9, NULL, '', '', NULL, '', NULL, 'prompt', NULL, 'prompt', '', NULL, 'prompt', 'yes', NULL, 'yes', 'Recode', 'Collector Code', '', NULL, NULL, NULL),
(20, 999, 9, '', '2018-05-22', 10, NULL, '', '', NULL, '', NULL, 'prompt', NULL, 'prompt', '', NULL, 'prompt', 'no', NULL, 'yes', 'Code', 'Clinical Code', '', NULL, NULL, NULL),
(21, 8451235, 12, 'test', '2018-05-07', 5, NULL, '', 'hgvjhgvbjh', NULL, '', NULL, 'yes', NULL, NULL, '', NULL, NULL, 'yes', NULL, 'yes', 'Recode', 'Collector Tweak', '', NULL, NULL, NULL),
(22, 152132, 15, 'test', '2018-05-14', 2, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'yes', NULL, 'yes', 'Code', 'Collector Tweak', '', NULL, NULL, NULL),
(24, 64654, 5, 'csc', '2018-07-23', 3, NULL, '', '', NULL, '', NULL, 'prompt', NULL, 'prompt', '', NULL, 'prompt', 'yes', NULL, 'prompt', 'Code', 'Collector Tweak', '', NULL, NULL, NULL),
(25, 87878, 76, 'tesrt', '2018-07-03', 5, NULL, '', '', NULL, '', NULL, 'prompt', NULL, 'prompt', '', NULL, 'prompt', 'yes', NULL, 'prompt', 'Code', 'Clinical Tweak', '', NULL, NULL, NULL),
(26, 878787, 776, 'abc', '2018-07-05', 9, NULL, 'test', '', NULL, 'altafhusseind ', '2018-07-09', 'yes', NULL, 'prompt', 'test', '2018-07-09', 'prompt', 'no', NULL, 'yes', 'Recode', 'Collector Code', '', NULL, NULL, '15.00'),
(27, 978896786, 756, '', '2018-07-02', 7, NULL, '', '', NULL, '', NULL, 'prompt', NULL, 'prompt', '', NULL, 'prompt', 'no', NULL, 'no', '', 'Collector Code', '', NULL, NULL, NULL),
(28, 3455, 556, 'test', '2018-07-05', 5, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', 'Code', 'Collector Code', 'Word', NULL, NULL, NULL),
(29, 23234254, 34, 'test', '2018-07-02', 4, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', 'Code', 'Clinical Tweak', 'Word', NULL, NULL, NULL),
(30, 5655656, 23, 'test', '2018-07-09', 2, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', '', 'Collector Code', 'Word', NULL, NULL, NULL),
(31, 11134, 11, 'vnbv', '2018-07-30', 67, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', '', '', '', NULL, NULL, NULL),
(32, 1119, 222, 'wer', '2018-07-23', 5, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', '', '', '', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `type_of_request`
--

CREATE TABLE `type_of_request` (
  `ID` int(11) NOT NULL,
  `request` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `type_of_request`
--

INSERT INTO `type_of_request` (`ID`, `request`) VALUES
(1, 'Collector Code'),
(2, 'Collector Tweak'),
(3, 'Clinical Code'),
(4, 'Clinical Tweak');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `firstname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `role` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `firstname`, `lastname`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `role`, `created_at`, `updated_at`) VALUES
(1, 'Admin', NULL, NULL, 'LbF6Kgp2hkFvp_2MdU4NCILyNn_77hNI', '$2y$13$tmqa7pMxHveldFwIf1l.CuvsazNH3uZVEmWxvW90Emzs20mZTutIW', NULL, 'admindemo@cybage.com', 10, 'admin', 1530098920, 1530098920),
(2, 'test', NULL, NULL, 'LbF6Kgp2hkFvp_2MdU4NCILyNn_77hNI', '$2y$13$tmqa7pMxHveldFwIf1l.CuvsazNH3uZVEmWxvW90Emzs20mZTutIW', NULL, 'test@test.com', 10, 'dev', 1530098920, 1530098920),
(4, 'altafhusseind ', 'Altafhussein', 'Devjani', 'LbF6Kgp2hkFvp_2MdU4NCILyNn_77hNI', '$2y$13$tmqa7pMxHveldFwIf1l.CuvsazNH3uZVEmWxvW90Emzs20mZTutIW', NULL, 'altafhusseind@cybage.com', 10, 'admin', 1530098920, 1530098920),
(5, 'amolsak    ', 'Amol', 'Sakore', 'LbF6Kgp2hkFvp_2MdU4NCILyNn_77hNI', '$2y$13$tmqa7pMxHveldFwIf1l.CuvsazNH3uZVEmWxvW90Emzs20mZTutIW', NULL, 'amolsak@cybage.com', 10, 'dev', 1530098920, 1530098920),
(6, 'atulga    ', 'Atul', 'Galande', 'LbF6Kgp2hkFvp_2MdU4NCILyNn_77hNI', '$2y$13$tmqa7pMxHveldFwIf1l.CuvsazNH3uZVEmWxvW90Emzs20mZTutIW', NULL, 'atulga@cybage.com', 10, 'admin', 1530098920, 1530098920),
(7, 'dilp    ', 'Dil', 'Pradhan', 'LbF6Kgp2hkFvp_2MdU4NCILyNn_77hNI', '$2y$13$tmqa7pMxHveldFwIf1l.CuvsazNH3uZVEmWxvW90Emzs20mZTutIW', NULL, 'dilp@cybage.com', 10, 'dev', 1530098920, 1530098920),
(8, 'harshalf    ', 'Harshal', 'Funde', 'LbF6Kgp2hkFvp_2MdU4NCILyNn_77hNI', '$2y$13$tmqa7pMxHveldFwIf1l.CuvsazNH3uZVEmWxvW90Emzs20mZTutIW', NULL, 'harshalf@cybage.com', 10, 'dev', 1530098920, 1530098920),
(9, 'manasit    ', 'Manasi', 'Tikam', 'LbF6Kgp2hkFvp_2MdU4NCILyNn_77hNI', '$2y$13$tmqa7pMxHveldFwIf1l.CuvsazNH3uZVEmWxvW90Emzs20mZTutIW', NULL, 'manasit@cybage.com', 10, 'dev', 1530098920, 1530098920),
(10, 'nikitaja    ', 'Nikita', 'Jayawant', 'LbF6Kgp2hkFvp_2MdU4NCILyNn_77hNI', '$2y$13$tmqa7pMxHveldFwIf1l.CuvsazNH3uZVEmWxvW90Emzs20mZTutIW', NULL, 'nikitaja@cybage.com', 10, 'dev', 1530098920, 1530098920),
(11, 'nishithr    ', 'Nishith', 'Rawal', 'LbF6Kgp2hkFvp_2MdU4NCILyNn_77hNI', '$2y$13$tmqa7pMxHveldFwIf1l.CuvsazNH3uZVEmWxvW90Emzs20mZTutIW', NULL, 'nishithr@cybage.com', 10, 'admin', 1530098920, 1530098920),
(12, 'pratikshash    ', 'Pratiksha', 'Shirbhate', 'LbF6Kgp2hkFvp_2MdU4NCILyNn_77hNI', '$2y$13$tmqa7pMxHveldFwIf1l.CuvsazNH3uZVEmWxvW90Emzs20mZTutIW', NULL, 'pratikshash@cybage.com', 10, 'dev', 1530098920, 1530098920),
(13, 'rameshwarg', 'Rameshwar', 'Gupta', 'LbF6Kgp2hkFvp_2MdU4NCILyNn_77hNI', '$2y$13$tmqa7pMxHveldFwIf1l.CuvsazNH3uZVEmWxvW90Emzs20mZTutIW', NULL, 'rameshwarg@cybage.com', 10, 'dev', 1530098920, 1530098920),
(14, 'samsong    ', 'Samson', 'Ghagare', 'LbF6Kgp2hkFvp_2MdU4NCILyNn_77hNI', '$2y$13$tmqa7pMxHveldFwIf1l.CuvsazNH3uZVEmWxvW90Emzs20mZTutIW', NULL, 'samsong@cybage.com', 10, 'dev', 1530098920, 1530098920),
(15, 'santoshma    ', 'Santosh', 'Mane', 'LbF6Kgp2hkFvp_2MdU4NCILyNn_77hNI', '$2y$13$tmqa7pMxHveldFwIf1l.CuvsazNH3uZVEmWxvW90Emzs20mZTutIW', NULL, 'santoshma@cybage.com', 10, 'admin', 1530098920, 1530098920),
(16, 'shashankp    ', 'Shashank', 'Pawar', 'LbF6Kgp2hkFvp_2MdU4NCILyNn_77hNI', '$2y$13$tmqa7pMxHveldFwIf1l.CuvsazNH3uZVEmWxvW90Emzs20mZTutIW', NULL, 'shashankp@cybage.com', 10, 'dev', 1530098920, 1530098920),
(17, 'shitalbar    ', 'Shital', 'Bargal', 'LbF6Kgp2hkFvp_2MdU4NCILyNn_77hNI', '$2y$13$tmqa7pMxHveldFwIf1l.CuvsazNH3uZVEmWxvW90Emzs20mZTutIW', NULL, 'shitalbar@cybage.com', 10, 'dev', 1530098920, 1530098920),
(18, 'shivshankarj    ', 'Shivshankar', 'Johari', 'LbF6Kgp2hkFvp_2MdU4NCILyNn_77hNI', '$2y$13$tmqa7pMxHveldFwIf1l.CuvsazNH3uZVEmWxvW90Emzs20mZTutIW', NULL, 'shivshankarj@cybage.com', 10, 'admin', 1530098920, 1530098920),
(19, 'sudhirbh    ', 'Sudhir', 'Bharambe', 'LbF6Kgp2hkFvp_2MdU4NCILyNn_77hNI', '$2y$13$tmqa7pMxHveldFwIf1l.CuvsazNH3uZVEmWxvW90Emzs20mZTutIW', NULL, 'sudhirbh@cybage.com', 10, 'admin', 1530098920, 1530098920),
(20, 'sunitasi    ', 'Sunita', 'Singh', 'LbF6Kgp2hkFvp_2MdU4NCILyNn_77hNI', '$2y$13$tmqa7pMxHveldFwIf1l.CuvsazNH3uZVEmWxvW90Emzs20mZTutIW', NULL, 'sunitasi@cybage.com', 10, 'dev', 1530098920, 1530098920),
(21, 'thoibisanad    ', 'Thoibisana', 'Devi', 'LbF6Kgp2hkFvp_2MdU4NCILyNn_77hNI', '$2y$13$tmqa7pMxHveldFwIf1l.CuvsazNH3uZVEmWxvW90Emzs20mZTutIW', NULL, 'thoibisanad@cybage.com', 10, 'admin', 1530098920, 1530098920),
(22, 'trikeshn    ', 'Trikesh', 'Naidu', 'LbF6Kgp2hkFvp_2MdU4NCILyNn_77hNI', '$2y$13$tmqa7pMxHveldFwIf1l.CuvsazNH3uZVEmWxvW90Emzs20mZTutIW', NULL, 'trikeshn@cybage.com', 10, 'dev', 1530098920, 1530098920),
(23, 'vishalsin    ', 'Vishal', 'Singh', 'LbF6Kgp2hkFvp_2MdU4NCILyNn_77hNI', '$2y$13$tmqa7pMxHveldFwIf1l.CuvsazNH3uZVEmWxvW90Emzs20mZTutIW', NULL, 'vishalsin@cybage.com', 10, 'dev', 1530098920, 1530098920),
(24, 'evonnelo    ', 'Evonne', 'Lobo', 'LbF6Kgp2hkFvp_2MdU4NCILyNn_77hNI', '$2y$13$tmqa7pMxHveldFwIf1l.CuvsazNH3uZVEmWxvW90Emzs20mZTutIW', NULL, 'evonnelo@cybage.com', 10, 'admin', 1530098920, 1530098920),
(25, 'sandeepge    ', 'Sandeep', 'Gera', 'LbF6Kgp2hkFvp_2MdU4NCILyNn_77hNI', '$2y$13$tmqa7pMxHveldFwIf1l.CuvsazNH3uZVEmWxvW90Emzs20mZTutIW', NULL, 'sandeepge@cybage.com', 10, 'admin', 1530098920, 1530098920);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `file_type`
--
ALTER TABLE `file_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `sdlc_phase`
--
ALTER TABLE `sdlc_phase`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `task`
--
ALTER TABLE `task`
  ADD PRIMARY KEY (`task_id`);

--
-- Indexes for table `type_of_request`
--
ALTER TABLE `type_of_request`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `file_type`
--
ALTER TABLE `file_type`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `sdlc_phase`
--
ALTER TABLE `sdlc_phase`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `task`
--
ALTER TABLE `task`
  MODIFY `task_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `type_of_request`
--
ALTER TABLE `type_of_request`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
