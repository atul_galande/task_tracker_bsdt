<?php
namespace frontend\controllers;
use Yii;
use frontend\models\Report;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\web\Request;

class ReportController extends Controller {
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex() {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        return $this->render('index', []);
    }

    public function actionGraph1() {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new Report();
        $request = new Request();
        $start_date=$request->post('start_date', '');
        $end_date=$request->post('end_date', '');
        $d = $model->getDataGraph1($start_date, $end_date);
        return $this->render('data-graph1', ['data' => $d, 'start_date'=>$start_date,'end_date'=>$end_date]);
    }
    
    public function actionGraph2() {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new Report();
        $request = new Request();
        $start_date=$request->post('start_date', '');
        $end_date=$request->post('end_date', '');
        $d = $model->getDataGraph2($start_date, $end_date);
        return $this->render('data-graph2', ['data' => $d,'start_date'=>$start_date,'end_date'=>$end_date]);
    }

    public function actionGraph3() {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new Report();
        $request = new Request();
        $start_date=$request->post('start_date', '');
        $end_date=$request->post('end_date', '');
        $d = $model->getDataGraph3($start_date, $end_date);
        return $this->render('data-graph3', ['data' => $d,'start_date'=>$start_date,'end_date'=>$end_date]);
    }
    
    public function actionGraph4() {
        $model = new Report();
        $d = $model->getDataGraph4();
        return $this->render('data-graph4', ['data' => $d,]);
    }
    
    public function actionGraph5() {
        $model = new Report();
        $d = $model->getDataGraph5();
        return $this->render('data-graph5', ['data' => $d,]);
    }
    
    public function actionGraph6() {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new Report();
        $request = new Request();
        $start_date=$request->post('start_date', '');
        $end_date=$request->post('end_date', '');
        $d = $model->getDataGraph6($start_date, $end_date);
        return $this->render('data-graph6', ['data' => $d,'start_date'=>$start_date,'end_date'=>$end_date]);
    }
    
    public function actionGraph7() {
        $model = new Report();
        $d = $model->getDataGraph7();
        return $this->render('data-graph7', ['data' => $d,]);
    }

    public function actionGraph8() {
        $model = new Report();
        $d = $model->getDataGraph8();
        return $this->render('data-graph8', ['data' => $d,]);
    }
    
    public function actionGraph9() {
        $model = new Report();
        $d = $model->getDataGraph9();
        return $this->render('data-graph9', ['data' => $d,]);
    }
    
    public function actionGraph10() {
        $model = new Report();
        $d = $model->getDataGraph10();
        return $this->render('data-graph10', ['data' => $d,]);
    }
    
    public function actionGraph11() {
        $model = new Report();
        $d = $model->getDataGraph11();
        return $this->render('data-graph11', ['data' => $d,]);
    }
}
