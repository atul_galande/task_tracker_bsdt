<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Task;
use frontend\models\TaskSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
//use yii\filters\AccessControl;
use yii\helpers\Html;

/**
 * TaskController implements the CRUD actions for Task model.
 */
class TaskController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [

		   'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Task models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model= new Task();
		$searchModel = new TaskSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$a= $model->getRole();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider, 'role'=>$a
        ]);
    }

    /**
     * Displays a single Task model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new Task();
		$a= $model->getListAssignedTo();
		$b= $model->getReviewer();
		$c= $model->getSDLCPhase();
		$d= $model->getTypeOfRequest();
		$e= $model->getAssigned();
		return $this->render('view', [
            'model' => $this->findModel($id),'ListAssignedTo'=>$a,'Reviewer'=>$b , 'ListOfSDLCPhase'=>$c, 'TypeOfRequest'=>$d, 'Assigned'=>$e
        ]);
    }

    /**
     * Creates a new Task model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new Task();

        if ($model->load(Yii::$app->request->post())) {
			$model->sdlc_phase= '';



			if($model->save()){
				return $this->redirect(['view', 'id' => $model->task_id]);
			}

        }
		$a= $model->getFileType();
		$b= $model->getReviewer();
		$c= $model->getSDLCPhase();
		$d= $model->getTypeOfRequest();
        return $this->render('create', [
            'model' => $model,   'Reviewer'=>$b, 'ListOfSDLCPhase'=>$c, 'TypeOfRequest'=>$d, 'FileType'=>$a
        ]
		);
    }

    /**
     * Updates an existing Task model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->task_id]);
        }
		$a= $model->getListAssignedTo();
		$b= $model->getReviewer();
		$c= $model->getSDLCPhase();
		$d= $model->getTypeOfRequest();
		$e= $model->getAssigned();
		$f= $model->getFileType();
        return $this->render('update', [
            'model' => $model, 'ListAssignedTo'=>$a,'Reviewer'=>$b , 'ListOfSDLCPhase'=>$c, 'TypeOfRequest'=>$d, 'Assigned'=>$e, 'FileType'=>$f
        ]);
    }

    /**
     * Deletes an existing Task model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

	 public function actionSendForReview($id)
    {
             if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
             $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
			       $model->sdlc_phase= 'Review';



			if($model->save()){
				return $this->redirect(['view', 'id' => $model->task_id]);
			}

        }
		$a= $model->getListAssignedTo();
		$c= $model->getSDLCPhase();
		$d= $model->getTypeOfRequest();
        return $this->render('send-for-review', [
            'model' => $model,'ListAssignedTo'=>$a, 'ListOfSDLCPhase'=>$c, 'TypeOfRequest'=>$d
        ]);
    }

	public function actionSendForRecode($id)
    {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
            $model = $this->findModel($id);


        if ($model->load(Yii::$app->request->post())) {
			$model->sdlc_phase= 'Recode';
			$model->review_done='yes';

			if($model->save()){
				return $this->redirect(['view', 'id' => $model->task_id]);
			}

        }
		$a= $model->getListAssignedTo();
		$c= $model->getSDLCPhase();
		$d= $model->getTypeOfRequest();
        return $this->render('send-for-recode', [
            'model' => $model,'ListAssignedTo'=>$a, 'ListOfSDLCPhase'=>$c, 'TypeOfRequest'=>$d
        ]);
	}

	public function actionAssigned($id)
    {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
            $model = $this->findModel($id);

		if ($model->load(Yii::$app->request->post())) {
			$model->sdlc_phase= 'Code';


			if($model->save()){
				return $this->redirect(['view', 'id' => $model->task_id]);
			}

        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->task_id]);
        }
		$a= $model->getListAssignedTo();
		$b= $model->getAssigned();
		$c= $model->getSDLCPhase();
		$d= $model->getTypeOfRequest();
        return $this->render('assigned', [
            'model' => $model,'ListAssignedTo'=>$a, 'Assigned'=>$b, 'ListOfSDLCPhase'=>$c, 'TypeOfRequest'=>$d
        ]);
	}

		 public function actionCloseTask($id)
    {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
                     $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
			$model->sdlc_phase= 'Resolved';
			$model->close='yes';


			if($model->save()){
				return $this->redirect(['view', 'id' => $model->task_id]);
			}
		}

		//\Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
		$d= $model->getTypeOfRequest();
		/*return [
                    'title' => "Create New Observation Category",
                    'content' =>$this->renderAjax('close-task', [
                        'model' => $model,'TypeOfRequest'=>$d
                    ]),
                    'footer' => Html::button('Close', ['class' => 'btn btn-default pull-right', 'data-dismiss' => "modal"]) .
                    Html::button('Save', ['class' => 'btn btn-primary pull-left', 'type' => "submit"])
        ];*/
        return $this->render('close-task', [
            'model' => $model,'TypeOfRequest'=>$d
        ]);
    }

		 public function actionResolved($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->task_id]);
        }

        return $this->render('resolved', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the Task model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Task the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Task::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

	public function actionGraph1()
    {
        $model = new Task();


		$d= $model->getDataGraph1();

        return $this->render('data-graph1', [
             'data'=>$d,
        ]);


    }
}
