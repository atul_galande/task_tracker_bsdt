<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\DashAsset;
use common\widgets\Alert;
DashAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <?= Html::csrfMetaTags() ?>
    <title>
        <?= Html::encode($this->title) ?>
    </title>
    <?php $this->head() ?>
</head>

<body>
    <?php $this->beginBody() ?>
    <div class="wrap">
		<?php
			NavBar::begin([
			'brandLabel' => Yii::$app->name,
			'brandUrl' => Yii::$app->homeUrl,
			'options' => [
			'class' => 'navbar-inverse navbar-fixed-top',
			],
			]);
			$menuItems = [
			['label' => 'Home', 'url' => ['/site/index']],
			//['label' => 'About', 'url' => ['/site/about']],
			//['label' => 'Task Tracker', 'url' => ['/task']],
			//['label' => 'Reports', 'url' => ['/report']],
			//['label' => 'Contact', 'url' => ['/site/contact']],
			];
			if (Yii::$app->user->isGuest) {
			//$menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
			$menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
			} else {
			$menuItems = [
			['label' => 'Task Tracker', 'url' => ['/task']],
			];
			if (Yii::$app->user->identity->role=='admin' ) {
			$menuItems[]=['label' => 'Reports', 'url' => ['/report']];
			}
			$menuItems[] = '<li>'
			. Html::beginForm(['/site/logout'], 'post')
			. Html::submitButton(
			'Logout (' . Yii::$app->user->identity->username . ')',
			['class' => 'btn btn-link logout']
			)
			. Html::endForm()
			. '</li>';
			}
			echo Nav::widget([
			'options' => ['class' => 'navbar-nav navbar-right'],
			'items' => $menuItems,
			]);
			NavBar::end();
		  ?>


		<div class="container">
			<?= Breadcrumbs::widget([
			'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
			]) ?>
			<?= Alert::widget() ?>
			<?= $content ?>
        </div>
    </div>

    <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>
