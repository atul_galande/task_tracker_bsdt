<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'BSDT Tracker';
?>
<div class="site-index">
	<div class="text-center">
		<?= Html::a('Get started with BSDT', ['task/index'], ['class' => 'btn btn-success']);?>
	</div>
</div>
<div class="row">
    <?= $this->render('dashboard'); ?>
</div>   
<div class="row">
    <?= $this->render('dashboardgraph1'); ?>
</div>
