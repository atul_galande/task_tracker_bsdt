<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use miloschuman\highcharts\Highcharts;
//use yii\captcha\Captcha;

//$this->title = 'Contact';
//$this->params['breadcrumbs'][] = $this->title;


?>
 <div class="body-content">
        <div class="row">
               <?=  
                     Highcharts::widget([
                       'options' => [
                          'chart'=>['type'=>'column'],

                          'title' => ['text' => 'Monthly Open Cases'],
                          'xAxis' => [
                             'categories' => ['Clinical Code', 'Clinical Tweak', 'Collector Code', 'Collector Tweak']
                          ],
                          'yAxis' => [
                             'title' => ['text' => 'Open Cases']
                          ],
                          'series' => [
                             ['name' => 'Jan', 'data' => [1, 0, 4, 5]],
                             ['name' => 'Feb', 'data' => [5, 7, 3, 14]],
                             ['name' => 'March', 'data' => [5, 7, 3, 30]],
                             ['name' => 'Apr', 'data' => [5, 7, 3, 6]]
                          ]
                       ]
                    ]);                       
                 ?> 
        </div>  
          <div class="row">
               <?=  
                     Highcharts::widget([
                       'options' => [
                          'chart'=>['type'=>'pie'],

                          'title' => ['text' => 'Monthly Open Cases'],
                           'series' => [
                                        ['type'=>'pie'],
                                        ['name'=>'open Cases'],
                                        ['data'=> [
                                                    ['Clinical Code',  45.0],
                                                    ['Clinical Tweak', 26.8],   
                                                    ['Collector Code', 8.5],
                                                    ['Collector Tweak',  6.2],
                                                  ]
                                        ]
                                    ]
                       ]
                    ]);                       
                 ?> 
        </div>  
           
</div>            

