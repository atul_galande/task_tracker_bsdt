<?php
use yii\helpers\Html;
use yii\grid\GridView;
use miloschuman\highcharts\Highcharts;
use frontend\models\Report;
use frontend\models\Task;
?>

<script src="https://code.highcharts.com/highcharts-3d.js"></script>

<?php
        $model = new Report();
        $start_date='';
        $end_date='';
        $data = $model->getDataGraph1($start_date, $end_date);
        
    $data_cases_closed = [];
    foreach ($data as $key => $row) {
        $data_cases_closed[] = [$row['type_of_request'], (int) $row['cases_closed']];
    }
    $data_pages_closed = [];
    foreach ($data as $key => $row) {
        $data_pages_closed[] = [$row['type_of_request'], (int) $row['pages_closed']];
    }
    
    $modeltask=new Task();
    $getleaderboard=$modeltask->getleaderboard();
?>
<div>
<div class="col-md-8">
      <div class=" panel panel-default">  
          <div class="panel-heading"><h3>Leader Board </h3></div>
    <div class="panel-body"><table class="table table-striped table-bordered">
        <tr><th>&nbsp</th><th>User</th><th>Open Task</th><th>Pages</th></tr>
        <?php foreach($getleaderboard as $row) { 
            
            echo "<tr><td>&nbsp</td><td>".$row['userfullname']."</td><td>".$row['count']."</td><td>".$row['pages']."</td></tr>";
        }
?>
        </table></div>
      </div>
</div>
    <div>
        <div class="panel panel-default col-md-4 pull-right">
    <div class="panel-body">
        <div id="container2"></div>
     </div>
</div>
<div class="row"></div>
<div class=" panel panel-default col-md-4 pull-right">
    <div class="panel-body">
        <div id="container1"></div>
    </div>
</div>
<div class="row"></div>
    </div>
</div>


<script>
    Highcharts.chart('container2', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            }
        },
         credits: {
              enabled: false
        },
        title: {
            text: 'Cases Closed'
        },
        credits: {
            enabled: false
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                depth: 35,
                dataLabels: {
                    enabled: false,
                    format: ' {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    },
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Cases Closed',
            colorByPoint: true,
            data: <?php echo json_encode($data_cases_closed) ?>
        }]
    });

    Highcharts.chart('container1', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            },
                //spacingTop: 0,
                //spacingBottom: 0,
                //spacingLeft: 0,
                //spacingRight: 0,
                //margin: [0, 100, 0, 0],
            
        },
       /* legend: {
                    enabled: true,
                    floating: true,
                    verticalAlign: 'middle',
                    align:'right',
                    layout: 'vertical',
                    //y: $(this).find('#container1').height()/4,
                },*/
         credits: {
              enabled: false
        },
        title: {
            text: 'Pages Closed'
        },
        credits: {
            enabled: false
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                depth: 35,
                size:'75%',
                dataLabels: {
                    enabled: false,
                    format: '{point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    },
                },
                
                showInLegend: true,
                
            }
        },
        series: [{
                name: 'Pages Closed',
                colorByPoint: true,
                data: <?php echo json_encode($data_pages_closed) ?>
        }]
    });
</script>
        	
