<?php
use frontend\models\Task;

    $model_Task=new Task();
    
    $TotalCountTask = $model_Task->getCountTotalTask();
    $TotalCloseTask = $model_Task->getTotalCloseTask();
    $TotalOpenTask = $model_Task->getTotalOpenTask();
    $TotalPagesCoded = $model_Task->getTotalPagesCoded();
?>
<style>
    .glyphicon { top:10px;}
    .content {
        min-height: 130px;
    }
    .wrap {padding: 0 0 40px;}
</style>

<div class="wrap">

  <!-- Content Wrapper. Contains page content -->

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Dashboard </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?= $TotalCountTask ?></h3>
               <p>Total Task</p>
            </div>
            <div class="icon">
              <span class="glyphicon glyphicon glyphicon-file"></span>
            </div>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?= $TotalCloseTask ?></h3>
              <p>Close Task</p>
            </div>
            <div class="icon">
              <span class="glyphicon glyphicon glyphicon-file"></span>
            </div>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?= $TotalOpenTask ?></h3>
              <p>Open Task</p>
            </div>
            <div class="icon">
              <span class="glyphicon glyphicon glyphicon-file"></span>
            </div>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><?= $TotalPagesCoded ?></h3>
              <p>Pages Coded</p>
            </div>
            <div class="icon">
              <span class="glyphicon glyphicon glyphicon-file"></span>
            </div>
          </div>
        </div>
        <!-- ./col -->
      </div>


    </section>

</div>

