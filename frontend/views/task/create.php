<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use dosamigos\datepicker\DatePicker;
/* @var $this yii\web\View */
/* @var $model frontend\models\Task */

$this->title = 'Create Task';
//$this->params['breadcrumbs'][] = ['label' => 'Tasks', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-create">
 <h1><?= Html::encode($this->title) ?></h1>
</div>
	
	<div class="task-form">

    <?php $form =yii\bootstrap\ActiveForm::begin([
    'layout' => 'default',
    'class' => 'form-horizontal',
    'fieldConfig' => [
    //'template' => "{input}{error}{label}",
    'horizontalCssClasses' => [
        'label' => 'col-sm-4',
        'offset' => 'col-sm-offset-4',
        'wrapper' => 'col-sm-8',
        'error' => '',
        'hint' => '',
    ],
]]
  ); ?>
        </div>

   <div class="panel panel-primary">
    <div class="panel-body">
        <fieldset>
            <legend>Info</legend>
            <div class="col-md-6">
		
		<?= $form->field($model, 'case_id')->textInput() ?>

    <?= $form->field($model, 'practice_id')->textInput() ?>

    <?= $form->field($model, 'case_manager')->textInput(['maxlength' => true]) ?>

    

    <?= $form->field($model, 'request_received_on')->widget(
							    DatePicker::className(), [
							         // inline too, not bad
							        'inline' => false, 
							         // modify template for custom rendering
							         // 'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
							        'clientOptions' => [
							            'autoclose' => true,
							            'format' => 'yyyy-mm-dd'
							        ]
							]);?>



    <?= $form->field($model, 'no_of_pages')->textInput() ?>

    <!--<?= $form->field($model, 'revised_pages')->textInput() ?>//-->

	<!--<?= $form->field($model, 'assigned_to')->textInput(['maxlength'=>true])?>//-->

		</div>
		<div class="col-md-6">
		<?= $form->field($model, 'note')->textArea(['maxlength' => true]) ?>

    <!--<?= $form->field($model, 'assigned_on')-> widget(
    							DatePicker::className(), [
							         // inline too, not bad
							        'inline' => false, 
							         // modify template for custom rendering
							         // 'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
							        'clientOptions' => [
							            'autoclose' => true,
							            'format' => 'dd-mm-yyyy'
							        ]
							]);?>//-->

	<?= $form->field($model, 'unplanned')->dropDownList(['no'=>'no','yes'=>'yes']); ?>
	
	
	
	<?= $form->field($model, 'type_of_request')->dropDownList($TypeOfRequest,['prompt'=>'Select Type Of Request']);?>
	
	<?= $form->field($model, 'file_type')->dropDownList($FileType,['prompt'=>'Select File Type']); ?>
	
	<!--<?= $form->field($model, 'estimated_time')->textInput(['maxlength' => true]) ?>//-->
					
	</div>

	
    </fieldset>
    <div class="form-group">
     <div class="pull-left ml-10">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
     </div>
        <div class="pull-left ml-10">
		<?= Html::a('Cancel', ['index', 'id' => $model->task_id], ['class' => 'btn btn-default']) ?>
    </div>
    </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
