<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;
/* @var $this yii\web\View */
/* @var $model frontend\models\Task */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="task-form">

    <?php $form =yii\bootstrap\ActiveForm::begin([
    'layout' => 'horizontal',
    'class' => 'form-horizontal',
    'fieldConfig' => [
    //'template' => "{input}{error}{label}",
    'horizontalCssClasses' => [
        'label' => 'col-sm-4',
        'offset' => 'col-sm-offset-4',
        'wrapper' => 'col-sm-7',
        'error' => '',
        'hint' => '',
    ],
]]
  ); ?>

    <div class="panel panel-default">
	<div class="panel-heading">Panel Heading</div>
	<div class="panel-body">
		<div class="col-md-6">
		
		<?= $form->field($model, 'case_id')->textInput() ?>

    <?= $form->field($model, 'practice_id')->textInput() ?>

    <?= $form->field($model, 'case_manager')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'request_received_on')->textInput() ?>

    <?= $form->field($model, 'no_of_pages')->textInput() ?>

    <?= $form->field($model, 'revised_pages')->textInput() ?>

	<?= $form->field($model, 'assigned_to')->dropdownList($ListAssignedTo,['prompt'=>'Select Assigned To']);?>

		</div>
		<div class="col-md-6">
		<?= $form->field($model, 'note')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'assigned_on')->dropDownList(['test'=>'test'],['prompt'=>'Select category']) ?>

	<?= $form->field($model, 'unplanned')->textInput(['maxlength' => true]) ?>
	
	<?= $form->field($model, 'sdlc_phase')->dropDownList($ListOfSDLCPhase,['prompt'=>'Select SDLC Phase']); ?>
	
	<?= $form->field($model, 'type_of_request')->dropDownList($TypeOfRequest,['prompt'=>'Select Type Of Request']);?>
	
	<?= $form->field($model, 'file_type')->textInput(['maxlength' => true]) ?>
	
	<?= $form->field($model, 'estimated_time')->textInput(['maxlength' => true]) ?>
	</div>

		</div>
	</div>
	
   <!-- <?= $form->field($model, 'reviewer')->dropDownList($Reviewer,['prompt'=>'Select Reviewer']) ?>

    <?= $form->field($model, 'review_assigned_on')->textInput() ?>

    <?= $form->field($model, 'review_done')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'review_done_on')->textInput() ?>

    <?= $form->field($model, 'send_for_review')->textInput(['maxlength' => true]) ?>

    
<hr class="mb-4"/>

    <div class="panel panel-default">
	<div class="panel-heading">Panel Heading</div>
	<div class="panel-body">
		<div class="col-md-6">
		<?= $form->field($model, 'recode_sent_to')->textInput(['maxlength' => true]) ?>
		 <?= $form->field($model, 'recode_sent_on')->textInput() ?>

    <?= $form->field($model, 'send_for_recode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'close')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'closed_date')->textInput()?>

    
		</div>
		<div class="col-md-6">
		  

    

    

    

    <?= $form->field($model, 'development_time')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'review_time')->textInput(['maxlength' => true]) ?>
		</div>
	</div>
	</div>
   <hr class="mb-4"/>//-->

  

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
