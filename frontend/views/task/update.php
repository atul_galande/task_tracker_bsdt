<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use dosamigos\datepicker\DatePicker;
$this->title = 'Update Task: '.$model->case_id;
?>
<div class="task-update">
    <h1><?= Html::encode($this->title) ?></h1>
</div>
<div class="task-form">
    <?php
        $form = yii\bootstrap\ActiveForm::begin([
                    'layout' => 'default',
                    'class' => 'form-horizontal',
                    'fieldConfig' => [
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-4',
                            'offset' => 'col-sm-offset-4',
                            'wrapper' => 'col-sm-8',
                            'error' => '',
                            'hint' => '',
                        ],
                    ]]
        );
    ?>
</div>
<div class="panel panel-primary">
    <div class="panel-body">
        <fieldset>
            <legend>Info</legend>
            <div class="col-md-6">
                <?= $form->field($model, 'case_id')->textInput() ?>
                <?= $form->field($model, 'practice_id')->textInput() ?>
                <?= $form->field($model, 'case_manager')->textInput(['maxlength' => true]) ?>
                <?=$form->field($model, 'request_received_on')->widget(
                                                                    DatePicker::className(), [
                                                                        'inline' => false,
                                                                        'clientOptions' => [
                                                                            'autoclose' => true,
                                                                            'format' => 'yyyy-mm-dd'
                                                                        ]
                                                                    ]);
                ?>
                <?= $form->field($model, 'no_of_pages')->textInput() ?>
                <?= $form->field($model, 'revised_pages')->textInput() ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'assigned_to')->dropDownList($ListAssignedTo, ['prompt' => 'Select Assigned To']); ?>
                <?=$form->field($model, 'assigned_on')->widget(
                                                            DatePicker::className(), [
                                                                'inline' => false,
                                                                'clientOptions' => [
                                                                    'autoclose' => true,
                                                                    'format' => 'yyyy-mm-dd'
                                                                ]
                                                            ]);
                ?>
                <?= $form->field($model, 'sdlc_phase')->dropDownList($ListOfSDLCPhase, ['prompt' => 'Select SDLC Phase']); ?>
                <?= $form->field($model, 'type_of_request')->dropDownList($TypeOfRequest, ['prompt' => 'Select Type Of Request']); ?>
                <?= $form->field($model, 'file_type')->dropDownList($FileType, ['prompt' => 'Select File Type']); ?>
                <?= $form->field($model, 'note')->textArea(['maxlength' => true]) ?>
            </div>
        </fieldset>
        <fieldset>
            <legend>Code</legend>
            <div class="col-md-6">
                <?= $form->field($model, 'recode_sent_to')->dropDownList($ListAssignedTo, ['prompt' => 'Select Recode Send To']); ?>
                <?=$form->field($model, 'recode_sent_on')->widget(
                                                                DatePicker::className(), [
                                                                    'inline' => false,
                                                                    'clientOptions' => [
                                                                        'autoclose' => true,
                                                                        'format' => 'yyyy-mm-dd'
                                                                    ]
                                                                ]);
                ?>
                <?= $form->field($model, 'send_for_recode')->dropDownList(['prompt' => 'Select Send For Recode', 'yes' => 'yes', 'no' => 'no']); ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'unplanned')->dropDownList(['no' => 'no', 'yes' => 'yes']); ?>
                <?= $form->field($model, 'estimated_time')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'development_time')->textInput(['maxlength' => true]) ?>
            </div>
        </fieldset>
        <fieldset>
            <legend>Review</legend>
            <div class="col-md-6">
                <?= $form->field($model, 'reviewer')->dropDownList($ListAssignedTo, ['prompt' => 'Select Reviewer']); ?>
                <?=$form->field($model, 'review_assigned_on')->widget(
                                                                    DatePicker::className(), [
                                                                        'inline' => false,
                                                                        'clientOptions' => [
                                                                            'autoclose' => true,
                                                                            'format' => 'yyyy-mm-dd'
                                                                        ]
                                                                    ]);
                ?>
                <?= $form->field($model, 'review_done')->dropDownList(['prompt' => 'Select Review Done', 'yes' => 'yes', 'no' => 'no']); ?>
            </div>
            <div class="col-md-6">
                <?=$form->field($model, 'review_done_on')->widget(
                                                                DatePicker::className(), [
                                                                    'inline' => false,
                                                                    'clientOptions' => [
                                                                        'autoclose' => true,
                                                                        'format' => 'yyyy-mm-dd'
                                                                    ]
                                                                ]);
                ?>
                <?= $form->field($model, 'send_for_review')->dropDownList(['prompt' => 'Select Send For Review', 'yes' => 'yes', 'no' => 'no']); ?>
                <?= $form->field($model, 'review_time')->textInput(['maxlength' => true]) ?>
            </div>
        </fieldset>
        <fieldset>
            <legend>Close</legend>
            <div class="col-md-6">
                <?= $form->field($model, 'close')->dropDownList(['prompt' => 'Select Close', 'yes' => 'yes', 'no' => 'no']); ?>
            </div>
            <div class="col-md-6">
                <?=$form->field($model, 'closed_date')->widget(
                                                            DatePicker::className(), [
                                                                'inline' => false,
                                                                'clientOptions' => [
                                                                    'autoclose' => true,
                                                                    'format' => 'yyyy-mm-dd'
                                                                ]
                                                            ]);
                ?>
            </div>
        </fieldset>
        <div class="form-group">
            <div class="pull-left ml-10">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            </div>
            <div class="pull-left ml-10">
            <?= Html::a('Cancel', ['view', 'id' => $model->task_id], ['class' => 'btn btn-default']) ?>
            </div>
        </div>                
    </div>
    <?php ActiveForm::end(); ?>
</div>
