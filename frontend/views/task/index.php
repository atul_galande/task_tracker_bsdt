<?php

use yii\helpers\Html;
use yii\grid\GridView;
use frontend\models\Roles;
//use kartik\grid\GridView;
//use kartik\export\ExportMenu;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\TaskSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tasks';
//$this->params['breadcrumbs'][] = $this->title;
?>
<?php 

if (Yii::$app->user->identity->role=='admin' ) { 
 $menuitems[]=['label' => 'Reports', 'url' => ['/report']];
}            
?>
<style>
.row{
  border: 1px dash red;
}
</style>
<div class="task-index">


 
  </div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
  <div class="row">
   <div class="col-md-6"><span class="col-md-6 h1"><?= Html::encode($this->title) ?></span></div>
   <div class="col-md-6 text-right"><p>
     <?php
     if(Roles::hasAdmin())
     {
       echo Html::a('Create Task', ['create'], ['class' => 'btn btn-success']);
     } ?>
   </p></div>
 </div>
 
 <?php 
  

  $gridColumns = [
        //['class' => 'yii\grid\SerialColumn'],
        'case_id',
        'request_received_on',
        'no_of_pages',
        'closed_date',
        'type_of_request',
        //['class' => 'yii\grid\ActionColumn'],
        // 'practice_id',
        'case_manager',
        //'revised_pages',
        //'assigned_to',
        //'note',
        //'assigned_on',
        //'reviewer',
        //'review_assigned_on',
        //'review_done',
        //'review_done_on',
        //'send_for_review',
        //'recode_sent_to',
        //'recode_sent_on',
        //'send_for_recode',
        //'close',
        //'closed_date',
        //'unplanned',
        //'sdlc_phase',
        //'type_of_request',
        //'file_type',
        //'estimated_time',
        //'development_time',
        //'review_time',
        //'role',
        //['class' => 'yii\grid\ActionColumn'],
  ];

  // Renders a export dropdown menu
  /*echo ExportMenu::widget([
      'dataProvider' => $dataProvider,
      'columns' => $gridColumns,
  ]);
*/
  ?>




 <?= GridView::widget([
  'dataProvider' => $dataProvider,
  'filterModel' => $searchModel,
  'columns' => [
    ['class' => 'yii\grid\SerialColumn'],

    [
     'attribute'=>'task_id',
     'format' => 'raw',
     'value'=>function ($data) {
      return Html::a($data->task_id,['task/view','id'=>$data->task_id]);
    },
    ],
  'case_id',
          // 'practice_id',
          // 'case_manager',
  'request_received_on',
  'no_of_pages',
        //'revised_pages',
  'assigned_to',
            //'note',
  'assigned_on',
  'reviewer',
            //'review_assigned_on',
            //'review_done',
            //'review_done_on',
            //'send_for_review',
            //'recode_sent_to',
            //'recode_sent_on',
            //'send_for_recode',
            //'close',
            //'closed_date',
            //'unplanned',
  'sdlc_phase',
            //'type_of_request',
            //'file_type',
            //'estimated_time',
            //'development_time',
            //'review_time',
             //'role',
            //['class' => 'yii\grid\ActionColumn'],

      [
          'class' => 'yii\grid\ActionColumn',
          'header' => 'Actions',
          'headerOptions' => ['style' => 'color:#337ab7'],
          'template' => '{view}{update}{delete}',
          'buttons' => [
            
            'view' => function ($url, $model) 
            {
              return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, 
              [
                  'title' => Yii::t('app', 'lead-view'),
              ]);
            },  
            'update' => function ($url, $model) 
            {
              if(Roles::hasAdmin()) 
              {
              return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, 
              [
              'title' => Yii::t('app', 'lead-update'),
              ]);
              }
            },  

            'delete' => function ($url, $model) {
              if(Roles::hasAdmin()) 
              { 

              return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url,

              [
              'title' => Yii::t('app', 'lead-delete'),
              'data' => [
              'confirm' => 'Are you sure you want to delete this item?',
              'method' => 'post',
              ],
              ]);
            }}, 
          ]
      ]
    ]
  ]);?>
