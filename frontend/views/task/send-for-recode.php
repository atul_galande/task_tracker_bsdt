
<?php
use yii\helpers\Html;
use dosamigos\datepicker\DatePicker;

// use yii\widgets\ActiveForm;

use yii\bootstrap\ActiveForm;
/* @var $this yii\web\View */
/* @var $model frontend\models\Task */
/* @var $form yii\widgets\ActiveForm */
/* @var $this yii\web\View */
/* @var $model frontend\models\Task */
$this->title = 'Send For Recode:';

// $this->params['breadcrumbs'][] = ['label' => 'Tasks', 'url' => ['index']];
// $this->params['breadcrumbs'][] = ['label' => $model->task_id, 'url' => ['view', 'id' => $model->task_id]];
// $this->params['breadcrumbs'][] = 'Send For Recode';

?>
<div class="task-update">

    <h1><?php echo Html::encode($this->title) ?></h1>

</div>

<div class="task-form">

    <?php
$form = yii\bootstrap\ActiveForm::begin(['layout' => 'default', 'class' => 'form-horizontal', 'fieldConfig' => [

// 'template' => "{input}{error}{label}",

'horizontalCssClasses' => ['label' => 'col-sm-4', 'offset' => 'col-sm-offset-4', 'wrapper' => 'col-sm-8', 'error' => '', 'hint' => '', ], ]]); ?>
</div>
	<div class="panel panel-primary">
    <div class="panel-body">
        <fieldset>
            <legend>Info</legend>
            <div class="col-md-6">
	<?php echo $form->field($model, 'case_id')->textInput(['readonly' => true]) ?>

    <?php echo $form->field($model, 'practice_id')->textInput(['readonly' => true]) ?>

    <?php echo $form->field($model, 'case_manager')->textInput(['maxlength' => true, 'readonly' => true]) ?>
	
	

    <?php echo $form->field($model, 'type_of_request')->textInput(['maxlength' => true, 'readonly' => true]) ?>
	</div>
	<div class="col-md-6">
		<?php echo $form->field($model, 'recode_sent_to')->dropDownList($ListAssignedTo, ['prompt' => 'Select Reviewer']); ?>
	
	<?php echo $form->field($model, 'recode_sent_on')->widget(
							    DatePicker::className(), [
							         // inline too, not bad
							        'inline' => false, 
							         // modify template for custom rendering
							         // 'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
							        'clientOptions' => [
							            'autoclose' => true,
							            'format' => 'yyyy-mm-dd'
							        ]
							]);?>
	
	
	
	<?php echo $form->field($model, 'review_time')->textInput(['maxlength' => true]) ?>
    <?php echo $form->field($model, 'note')->textArea(['maxlength' => true]) ?>

	
	</div>

	
    </fieldset>

    <div class="form-group">
        <div class="pull-left ml-10">
        <?php echo Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            </div>
        <div class="pull-left ml-10">
		<?php echo Html::a('Cancel', ['view', 'id' => $model->task_id], ['class' => 'btn btn-default']) ?>
    </div>
    </div>
    </div>

    <?php
ActiveForm::end(); ?>

</div>
