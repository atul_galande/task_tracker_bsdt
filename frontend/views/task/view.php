<?php
//use johnitvn\ajaxcrud\CrudAsset;
use yii\helpers\Html;
//use yii\widgets\DetailView;
use yii\bootstrap\ActiveForm;
use frontend\models\Roles;
//CrudAsset::register($this);
/* @var $this yii\web\View */
/* @var $model frontend\models\Task */

//$this->title = $model->task_id;
//$this->params['breadcrumbs'][] = ['label' => 'Tasks', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">

        <?php
        if(empty($model->sdlc_phase) && Roles::hasAdmin()){
        echo'<div class="pull-left ml-10">';
			echo Html::a('Assign', ['assigned', 'id' => $model->task_id], ['class' => 'btn btn-primary']);
                       echo '</div>';
        }
		 if(($model->sdlc_phase=="Code" || $model->sdlc_phase=="Recode"  )&&  Yii::$app->user->identity->username==$model->assigned_to){
			echo'<div class="pull-left ml-10">';
                     echo Html::a('Review', ['send-for-review', 'id' => $model->task_id], ['class' => 'btn btn-info']);
		echo '</div>';	
		}
		 if($model->sdlc_phase=="Review" && Yii::$app->user->identity->username==$model->reviewer){
			echo'<div class="pull-left ml-10">';
                     echo Html::a('Recode', ['send-for-recode', 'id' => $model->task_id], ['class' => 'btn btn-info']);
			echo '</div>';
                     echo'<div class="pull-left ml-10">';
			echo Html::a('Close', ['close-task', 'id'=> $model->task_id], ['class'=>'btn btn-info']);
        echo '</div>';
		}
		
                if(Roles::hasAdmin())
		{
                    echo'<div class="pull-left ml-10">'; 
                    echo Html::a('Update', ['update', 'id' => $model->task_id], ['class' => 'btn btn-primary']); 
                    echo '</div>'; 
                    echo'<div class="pull-left ml-10">';
                     echo Html::a('Delete', ['delete', 'id' => $model->task_id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]);
                     echo '</div>';
                 }
                 ?>

</div>


<div class="task-form">

    <?php $form =yii\bootstrap\ActiveForm::begin([
    'layout' => 'default',
    'class' => 'form-horizontal',
    'fieldConfig' => [
    //'template' => "{input}{error}{label}",
    'horizontalCssClasses' => [
        'label' => 'col-sm-4',
        'offset' => 'col-sm-offset-4',
        'wrapper' => 'col-sm-7',
        'error' => '',
        'hint' => '',
    ],
]]
  ); ?>
</div>

   <div class="panel panel-primary">
    <div class="panel-body">
        <fieldset>
            <legend>Info</legend>
            <div class="col-md-6">
	<?= $form->field($model, 'task_id')->textInput([$Assigned, 'readonly'=>true]) ?>
		
	<?= $form->field($model, 'case_id')->textInput(['readonly'=>true]) ?>

    <?= $form->field($model, 'practice_id')->textInput(['readonly'=>true]) ?>

    <?= $form->field($model, 'case_manager')->textInput(['maxlength' => true, 'readonly'=>true]) ?>
	
	 <?= $form->field($model, 'request_received_on')->textInput(['readonly'=>true]) ?>
	 
	    <?= $form->field($model, 'no_of_pages')->textInput(['readonly'=>true]) ?>

    <?= $form->field($model, 'revised_pages')->textInput(['readonly'=>true]) ?>
	
	</div>
	<div class="col-md-6">
   

 

	<?= $form->field($model, 'assigned_to')->textInput(['maxlength' => true, 'readonly'=>true])?>
            
    <?= $form->field($model, 'assigned_on')->textInput(['maxlength' => true, 'readonly'=>true]) ?>
	
	<?= $form->field($model, 'sdlc_phase')->textInput(['maxlength' => true, 'readonly'=>true]) ?>
	
	<?= $form->field($model, 'type_of_request')->textInput(['maxlength' => true, 'readonly'=>true])?>
	
	<?= $form->field($model, 'file_type')->textInput(['maxlength' => true, 'readonly'=>true]) ?>
	
	<?= $form->field($model, 'note')->textArea(['maxlength' => true, 'readonly'=>true]) ?>

 </div>
        </fieldset>
        <fieldset>
            <legend>Code</legend>
            <div class="col-md-6">
		<?= $form->field($model, 'recode_sent_to')->textInput(['maxlength' => true, 'readonly'=>true])?>
	
	<?= $form->field($model, 'recode_sent_on')->textInput(['readonly'=>true]) ?>
	
	<?= $form->field($model, 'send_for_recode')->textInput(['maxlength' => true, 'readonly'=>true]) ?>

	

	
	
	</div>
	
	<div class="col-md-6">
	
    
	
	
	<?= $form->field($model, 'unplanned')->textInput(['maxlength' => true, 'readonly'=>true]) ?>
	
	<?= $form->field($model, 'estimated_time')->textInput(['maxlength' => true, 'readonly'=>true]) ?>
	
	<?= $form->field($model, 'development_time')->textInput(['maxlength' => true, 'readonly'=>true]) ?>
	 </div>
        </fieldset>
        <fieldset>
            <legend>Review</legend>
            <div class="col-md-6">
		<?= $form->field($model, 'reviewer')->textInput(['maxlength' => true, 'readonly'=>true]) ?>
	
	<?= $form->field($model, 'review_assigned_on')->textInput(['readonly'=>true]) ?>

    <?= $form->field($model, 'review_done')->textInput(['maxlength' => true, 'readonly'=>true]) ?>
	</div>
	<div class="col-md-6">

    <?= $form->field($model, 'review_done_on')->textInput(['readonly'=>true]) ?>
	
	<?= $form->field($model, 'send_for_review')->textInput(['maxlength' => true, 'readonly'=>true]) ?>
	
	
	
	<?= $form->field($model, 'review_time')->textInput(['maxlength' => true, 'readonly'=>true]) ?>
	</div>

        </fieldset>
        <fieldset>
            <legend>Close</legend>
            <div class="col-md-6">
	<?= $form->field($model, 'close')->textInput(['maxlength' => true, 'readonly'=>true]) ?>
</div>
<div class="col-md-6">
    <?= $form->field($model, 'closed_date')->textInput(['readonly'=>true])?>
	</div>
	</div>
	
	
	
	<?php ActiveForm::end(); ?>

    <!--DetailView::widget([
        'model' => $model,
        'attributes' => [
            'task_id',
            'case_id',
            'practice_id',
            'case_manager',
            'request_received_on',
            'no_of_pages',
            'revised_pages',
            'assigned_to',
            'note',
            'assigned_on',
            'reviewer',
            'review_assigned_on',
            'review_done',
            'review_done_on',
            'send_for_review',
            'recode_sent_to',
            'recode_sent_on',
            'send_for_recode',
            'close',
            'closed_date',
			'sdlc_phase',
            'unplanned',
            'type_of_request',
            'file_type',
            'estimated_time',
            'development_time',
            'review_time',
        ],
    ]) //-->

</div>
