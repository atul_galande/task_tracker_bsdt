<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\TaskSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="task-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'task_id') ?>

    <?= $form->field($model, 'case_id') ?>

    <?= $form->field($model, 'practice_id') ?>

    <?= $form->field($model, 'case_manager') ?>

    <?= $form->field($model, 'request_received_on') ?>

    <?php // echo $form->field($model, 'no_of_pages') ?>

    <?php // echo $form->field($model, 'revised_pages') ?>

    <?php // echo $form->field($model, 'assigned_to') ?>

    <?php // echo $form->field($model, 'note') ?>

    <?php // echo $form->field($model, 'assigned_on') ?>

    <?php // echo $form->field($model, 'reviewer') ?>

    <?php // echo $form->field($model, 'review_assigned_on') ?>

    <?php // echo $form->field($model, 'review_done') ?>

    <?php // echo $form->field($model, 'review_done_on') ?>

    <?php // echo $form->field($model, 'send_for_review') ?>

    <?php // echo $form->field($model, 'recode_sent_to') ?>

    <?php // echo $form->field($model, 'recode_sent_on') ?>

    <?php // echo $form->field($model, 'send_for_recode') ?>

    <?php // echo $form->field($model, 'close') ?>

    <?php // echo $form->field($model, 'closed_date') ?>

    <?php // echo $form->field($model, 'unplanned') ?>

    <?php // echo $form->field($model, 'sdlc_phase') ?>

    <?php // echo $form->field($model, 'type_of_request') ?>

    <?php // echo $form->field($model, 'file_type') ?>

    <?php // echo $form->field($model, 'estimated_time') ?>

    <?php // echo $form->field($model, 'development_time') ?>

    <?php // echo $form->field($model, 'review_time') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
