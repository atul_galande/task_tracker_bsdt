<?php
    use yii\helpers\Html;
    use yii\grid\GridView;
    use miloschuman\highcharts\Highcharts;
?>

<?php
    $unplanned = [];
    foreach ($data as $key => $row) {
        $unplanned[] = [$row['mondaythisweek'], (int) $row['unplanned']];
    }
    $planned = [];
    foreach ($data as $key => $row) {
        $planned[] = [$row['mondaythisweek'], (int) $row['planned']];
    }
    $data_pages_closed = [];
    foreach ($data as $key => $row) {
        $data_pages_closed[] = [$row['mondaythisweek'], (int) $row['pages_closed']];
    }
?>

<div class="panel panel-default">
    <div class="panel-heading"><h3 class="panel-title">BSDT: Trend of Tasks Planned (Salesforce) vs Unplanned (via Email)</h3></div>
    <div class="panel-body">
        <?= $this->render('form', ['action'=>['report/graph3'], 'start_date'=>$start_date, 'end_date'=>$end_date])?>
        <div id="container1" class="col-md-12 panel panel-default "></div>
        <table class='table table-striped table-bordered'>
            <tr>
                <th></th>
                <?php
                    foreach ($data as $key => $row) {
                        echo"<th width=10%>";
                        echo $row['mondaythisweek'];
                        echo"</th>";
                }
                ?>
            </tr>
            <tr>
                <th width=10%>via SF</th>
                <?php
                    foreach ($data as $key => $row) {
                        echo"<td width=10%>";
                        echo $row['planned'];
                        echo"</td>";
                    }
                ?>
            </tr>
            <tr>
                <th>via Email</th>
                <?php
                    foreach ($data as $key => $row) {
                        echo"<td width=10%>" . $row['unplanned'] . "</td>";
                    }
                ?>
            </tr>
        </table>

    
    </div>
</div> 

<script>
    Highcharts.chart('container1', {
        chart: {
            type: 'spline'
        },
        title: {
            text: 'Trend of Tasks (Planned (Salesforce) vs Unplanned (via Email)'
        },
        credits: {
              enabled: false
        },
        xAxis: {
            categories:<?php echo json_encode(  $data_pages_closed) ?>
        },
        yAxis: {
            title: {
                text: 'No of pages coded'
            },
            labels: {
                formatter: function () {
                    return this.value + '';
                }
            }
        },
        tooltip: {
            crosshairs: true,
            shared: true
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            spline: {
                marker: {
                    radius:0,
                    lineColor: '#000',
                    lineWidth: 0
                }
            }
        },
        series: [{
                name: 'Unplanned',
                color:"red",
                data: <?php echo json_encode( $unplanned) ?>
            }, 
            {
                name: 'Planned',
                color:"blue",
                data: <?php echo json_encode( $planned) ?>
        }]
    });
</script>
