<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
?>
<style>
    .ml-10{
        margin-left:10px;
    }
</style>

<div class="row">
    <?= Html::beginForm($action, 'POST');?>
    <div class="form-group field-task-practice_id col-md-12">
        <label class="control-label pull-left" >Start Date</label>
        <div class="col-md-2 pull-left ml-10">
            <input type="date" class="form-control " name="start_date" value="<?= $start_date?>">
        </div>
        <label class="control-label pull-left ml-10" >End Date</label>
        <div class="col-md-2 pull-left ml-10">
            <input type="date"  class="form-control" name="end_date" value="<?= $end_date?>">
        </div>
        <div class="form-group col-sm-2">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-success']) ?>
    </div>
    </div>
<?= Html::endForm();?>
</div>