<?php
use yii\helpers\Html;
use yii\grid\GridView;
use miloschuman\highcharts\Highcharts;
?>

<script src="https://code.highcharts.com/highcharts-3d.js"></script>

<?php
    foreach ($data as $key => $row) {
        $small= $row['small_task'];
        $medium= $row['medium_task'];
        $large= $row['large_task'];
    }
?>

<div class="panel panel-default">
    <div class="panel-heading"><h3 class="panel-title"> Task Size</h3></div>
    <div class="panel-body">
        <?= $this->render('form', ['action'=>['report/graph6'], 'start_date'=>$start_date, 'end_date'=>$end_date])?>
        <div id="container1" class="col-md-12 panel panel-default "></div>
        <table class='table table-striped table-bordered'>
            <tr>
                <th>&nbsp;</th>
                <th>Small</th>
                <th>Medium</th>
                <th>Large</th>
            </tr>
            <tr>
                <th>Task Size</th>
                <?php
                    foreach ($data as $key => $row) {
                        echo"<td width=25%>";
                        echo $row['small_task'];
                        echo"</td>";
                    }
                ?>

                <?php
                    $total = 0;
                    foreach ($data as $key => $row) {
                        echo"<td width=25%>";
                        echo $row['medium_task'];
                        echo"</td>";
                    }
                ?>

                <?php
                    foreach ($data as $key => $row) {
                        echo"<td width=25%>";
                        echo $row['large_task'];
                        echo"</td>";
                    }
                ?>
            </tr>
        </table>

        
    </div>
</div>

<script>
    Highcharts.chart('container1', {
        chart: {
            type: 'column',
            options3d: {
                enabled: true,
                alpha: 0,
                beta: 0,
                depth: 20,
                viewDistance: 25
            }
        },
         credits: {
              enabled: false
        },
        title: {
            text: 'Task Size'
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: 'Total Pages'
            }
        },
        legend: {
            enabled: false
        },
        plotOptions: {
            column: {
                depth: 15
            },
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}%'
                }
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
        },
        credits: {
            enabled: false
        },
        "series": [
            {
                "name": "Task Size",
                "data": [
                    {
                        "name": "Small",
                        "y": <?php echo $small ?>,
                    },
                    {
                        "name": "Medium", 
                        "y": <?php echo $medium ?>,
                    },
                    {
                        "name": "Large",
                        "y": <?php echo $large ?>,
                    }
                ]
            }
        ]
    });
</script>

