<?php
use yii\helpers\Html;

$this->title = 'Reports';
?>

<div class="report-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="row">
        <ul class="list-group">
            <li class="list-group-item"><?= Html::a('Report 1 - BSDT: Cases V/S Pages Closed', ['graph1'], ['class' => ' ']) ?></li>
            <li class="list-group-item"><?= Html::a('Report 2 - BSDT: Trend-Closed Cases v/s Closed Pages', ['graph2'], ['class' => ' ']) ?></li>
            <li class="list-group-item"><?= Html::a('Report 3 - BSDT: Trend of Tasks Planned (Salesforce) vs Unplanned (via Email)', ['graph3'], ['class' => ' ']) ?></li>
            <!--<li class="list-group-item"><?= Html::a('Report 4 - BSDT: Individual Productivity ( Task Wise – Page Coded )', ['graph4'], ['class' => ' ']) ?></li>
            <li class="list-group-item"><?= Html::a('Report 5 - BSDT: Individual Productivity ( Task Wise – Page Reviewed)', ['graph5'], ['class' => ' ']) ?></li>//-->
            <li class="list-group-item"><?= Html::a('Report 4 - BSDT: Task Size', ['graph6'], ['class' => ' ']) ?></li>
            <!--<li class="list-group-item"><?= Html::a('Report 7 - BSDT: Details of Case/Pages received (Daily Inflow/Outflow)', ['graph7'], ['class' => ' ']) ?></li>
            <li class="list-group-item"><?= Html::a('Report 8 - BSDT: Team Utilization', ['graph8'], ['class' => ' ']) ?></li>
            <li class="list-group-item"><?= Html::a('Report 9 - BSDT: Team Utilization', ['graph9'], ['class' => ' ']) ?></li>
            <li class="list-group-item"><?= Html::a('Report 10 - BSDT: Individual Productivity', ['graph10'], ['class' => ' ']) ?></li>
            <li class="list-group-item"><?= Html::a('Report 11 - BSDT: Individual Productivity', ['graph11'], ['class' => ' ']) ?></li>//-->
        </ul>
    </div>
</div>
