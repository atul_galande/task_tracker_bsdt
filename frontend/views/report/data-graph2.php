<?php
use yii\helpers\Html;
use yii\grid\GridView;
use miloschuman\highcharts\Highcharts;
?>

<?php
    $categories= [];
    foreach ($data as $key => $row) {
        $categories[] = $row['type_of_request'];
    }
    $data_cases_closed = [];
    foreach ($data as $key => $row) {
        $data_cases_closed[] = [$row['type_of_request'], (int) $row['cases_closed']];
    }
    $data_pages_closed = [];
    foreach ($data as $key => $row) {
        $data_pages_closed[] = [$row['type_of_request'], (int) $row['pages_closed']];
    }
?>

<div class="panel panel-default">
    <div class="panel-heading"><h3 class="panel-title"> BSDT: Trend – Closed Cases v/s Closed Pages</h3></div>
    <div class="panel-body">
        <?= $this->render('form', ['action'=>['report/graph2'], 'start_date'=>$start_date, 'end_date'=>$end_date])?>
        <div id="container1" class="col-md-12 panel panel-default "></div>
        <table class='table table-striped table-bordered'>
            <tr>
                <th></th>
                <?php
                    foreach ($data as $key => $row) {
                        echo"<tH width=20%>";
                        echo $row['type_of_request'];
                        echo"</th>";
                    }
                ?>
            </tr>
            <tr>
                <th>Cases closed</th>
                <?php
                    $total = 0;
                    foreach ($data as $key => $row) {
                        echo"<td width=20%>";
                        echo $row['cases_closed'];
                        echo"</td>";
                    }
                ?>
            </tr>
            <tr>
                <th>Pages closed</th>
                <?php
                    $total1 = 0;
                    foreach ($data as $key => $row) {
                        echo"<td width=20%>" . $row['pages_closed'] . "</td>";
                    }
                ?>
            </tr>
        </table>

      
    </div>
</div>

<script>
    Highcharts.chart('container1', {
        chart: {
            type: 'spline'
        },
        title: {
            text: 'Closed Cases v/s Closed Pages'
        },
        credits: {
              enabled: false
        },
        xAxis: {
            categories:<?php echo json_encode($categories) ?>
        },
        yAxis: {
            title: {
                text: 'Count'
            },
            labels: {
                formatter: function () {
                    return this.value + '';
                }
            }
        },
        tooltip: {
            crosshairs: true,
            shared: true
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            spline: {
                marker: {
                    radius: 0,
                    lineColor: '#000',
                    lineWidth: 0
                }
            }
        },
        series: [{
            name: 'Cases Closed',
            color: "blue",
            data: <?php echo json_encode($data_cases_closed) ?>
            },
            {
                name: 'Pages Closed',
                color: "red",
                data: <?php echo json_encode($data_pages_closed) ?>
        }]
    });
</script>
