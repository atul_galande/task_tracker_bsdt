<?php
use yii\helpers\Html;
use yii\grid\GridView;
use miloschuman\highcharts\Highcharts;
?>

<script src="https://code.highcharts.com/highcharts-3d.js"></script>

<?php
    $data_cases_closed = [];
    foreach ($data as $key => $row) {
        $data_cases_closed[] = [$row['type_of_request'], (int) $row['cases_closed']];
    }
    $data_pages_closed = [];
    foreach ($data as $key => $row) {
        $data_pages_closed[] = [$row['type_of_request'], (int) $row['pages_closed']];
    }
?>

<div class="panel panel-default">
    <div class="panel-heading"><h3 class="panel-title">BSDT: Cases v/s Pages Closed</h3></div>
    <div class="panel-body">
        <?= $this->render('form', ['action'=>['report/graph1'], 'start_date'=>$start_date, 'end_date'=>$end_date])?>
        <div id="container2" class="col-md-6 panel panel-default pull-left"></div>
        <div id="container1" class="col-md-6 panel panel-default pull-right"></div>
        <table class='table table-striped table-bordered'>
            <tr>
                <th>Cases v/s Pages closed</th>
                <?php
                    foreach ($data as $key => $row) {
                        echo"<th width=16%>";
                        echo $row['type_of_request'];
                        echo"</th>";
                    }
                ?>
                <th width=18%> Total </th>
            </tr>
            <tr>
                <th>Cases closed</th>
                <?php
                    $total = 0;
                    foreach ($data as $key => $row) {
                        echo"<td width=16%>";
                        echo $row['cases_closed'];
                        echo"</td>";
                        $total = $row['cases_closed'] + $total;
                    }
                ?>
                <td width=18%> 
                    <?= $total ?>
                </td>
            </tr>
            <tr>
                <th>Pages closed</th>
                <?php
                    $total1 = 0;
                    foreach ($data as $key => $row) {
                        echo"<td width=16%>" . $row['pages_closed'] . "</td>";
                        $total1 = $row['pages_closed'] + $total1;
                    }
                ?>
                <td width=18%>
                    <?= $total1 ?>
                </td>
            </tr>
        </table>	

      
    </div>
</div>

<script>
    Highcharts.chart('container2', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            }
        },
         credits: {
              enabled: false
        },
        title: {
            text: 'Cases Closed'
        },
        credits: {
            enabled: false
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                depth: 35,
                dataLabels: {
                    enabled: true,
                    format: ' {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    },
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Cases Closed',
            colorByPoint: true,
            data: <?php echo json_encode($data_cases_closed) ?>
        }]
    });

    Highcharts.chart('container1', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            }
        },
         credits: {
              enabled: false
        },
        title: {
            text: 'Pages Closed'
        },
        credits: {
            enabled: false
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                depth: 35,
                dataLabels: {
                    enabled: true,
                    format: '{point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    },
                },
                showInLegend: true
            }
        },
        series: [{
                name: 'Pages Closed',
                colorByPoint: true,
                data: <?php echo json_encode($data_pages_closed) ?>
        }]
    });
</script>
        	
