<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class DashAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
    'css/site.css',
		'css/_all-skins.min.css',
		'css/AdminLTE.min.css',
		//'css/bootstrap.min.css',
		//'css/bootstrap-theme.min.css',
		'css/font-awesome.min.css',
		'css/ionicons.min',
    ];
    public $js = [
		'js/adminlte.js',
		'js/adminlte.min.js',
		'js/bootstrap.min.js',
		'js/dashboard.js',
		'js/demo.js',
		'js/jquery.min.js',
		'js/jquery.sparkline.min.js',
		'js/jquery-ui.min.js',
		'js/morris.min.js',
		'js/raphael.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
