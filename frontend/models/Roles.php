<?php
namespace frontend\models;
use yii;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Roles {
    public static function hasAdmin()
    {
        if(Yii::$app->user->isGuest )
        {
            return false;
        }
         if (Yii::$app->user->identity->role=='admin' )
         { 
             return true;
         }else 
         {
             return false;
         }
    }
    public static function hasDev()
    {
        if(Yii::$app->user->isGuest )
        {
            return false;
        }
         if (Yii::$app->user->identity->role=='dev' )
         { 
             return true;
         }else 
         {
             return false;
         }
    }
}
?>