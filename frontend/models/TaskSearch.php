<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Task;

/**
 * TaskSearch represents the model behind the search form of `frontend\models\Task`.
 */
class TaskSearch extends Task
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['task_id', 'case_id', 'practice_id', 'no_of_pages', 'revised_pages'], 'integer'],
            [['case_manager', 'request_received_on', 'assigned_to', 'note', 'assigned_on', 'reviewer', 'review_assigned_on', 'review_done', 'review_done_on', 'send_for_review', 'recode_sent_to', 'recode_sent_on', 'send_for_recode', 'close', 'closed_date', 'unplanned', 'sdlc_phase', 'type_of_request', 'file_type'], 'safe'],
            [['estimated_time', 'development_time', 'review_time'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {   $query = Task::find();
        if(Roles::hasAdmin()){
            $where = [];
        } 
        if(Roles::hasDev()){
            $username = Yii::$app->user->identity->username;
            $query->where(['assigned_to' => $username]);
            $query->orWhere(['reviewer' => $username]);
        }
        
        
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
        'pageSize' => 10,
    ],
            
        ]);
        $params['assigned_to'] = Yii::$app->user->identity->username;
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'task_id' => $this->task_id,
            'case_id' => $this->case_id,
            'practice_id' => $this->practice_id,
            'request_received_on' => $this->request_received_on,
            'no_of_pages' => $this->no_of_pages,
            'revised_pages' => $this->revised_pages,
            'assigned_on' => $this->assigned_on,
            'review_assigned_on' => $this->review_assigned_on,
            'review_done_on' => $this->review_done_on,
            'recode_sent_on' => $this->recode_sent_on,
            'closed_date' => $this->closed_date,
            'estimated_time' => $this->estimated_time,
            'development_time' => $this->development_time,
            'review_time' => $this->review_time,
        ]);

        $query->andFilterWhere(['like', 'case_manager', $this->case_manager])
            ->andFilterWhere(['like', 'assigned_to', $this->assigned_to])
            ->andFilterWhere(['like', 'note', $this->note])
            ->andFilterWhere(['like', 'reviewer', $this->reviewer])
            ->andFilterWhere(['like', 'review_done', $this->review_done])
            ->andFilterWhere(['like', 'send_for_review', $this->send_for_review])
            ->andFilterWhere(['like', 'recode_sent_to', $this->recode_sent_to])
            ->andFilterWhere(['like', 'send_for_recode', $this->send_for_recode])
            ->andFilterWhere(['like', 'close', $this->close])
            ->andFilterWhere(['like', 'unplanned', $this->unplanned])
            ->andFilterWhere(['like', 'sdlc_phase', $this->sdlc_phase])
            ->andFilterWhere(['like', 'type_of_request', $this->type_of_request])
            ->andFilterWhere(['like', 'file_type', $this->file_type]);

        return $dataProvider;
    }
}
