<?php
namespace frontend\models;
use yii\db\Query;
use Yii;

class Report extends \yii\db\ActiveRecord {
    public static function tableName(){
        return 'task';
    }
	
    public function getDataGraph1($start_date, $end_date){
        $query= new Query();
        $query->SELECT(['type_of_request',"count(task_id) as cases_closed","sum(no_of_pages) as pages_closed"])
                    ->from('task')
                    ->where("close='yes'");
                    if(!empty($start_date)){
                        
                        $query->andWhere(['>=','request_received_on',date('Y-m-d',strtotime($start_date) )]);
                    }
                    if(!empty($end_date)){
                        $query->andWhere(['<=','request_received_on',date('Y-m-d',strtotime($end_date) )]);
                    }
                    $query->groupby('type_of_request');
                    $data = $query->all();
        return $data;
    }
        
    public function getDataGraph2($start_date, $end_date){
        $query= new Query();
        $query->SELECT(['type_of_request',"count(task_id) as cases_closed","sum(no_of_pages) as pages_closed"])
                    ->from('task')
                    ->where("close='yes'");
                    if(!empty($start_date)){
                        
                        $query->andWhere(['>=','request_received_on',date('Y-m-d',strtotime($start_date) )]);
                    }
                    if(!empty($end_date)){
                        $query->andWhere(['<=','request_received_on',date('Y-m-d',strtotime($end_date) )]);
                    }
                    $query->groupby('type_of_request');
                    $data=$query->all();
        return $data;
    }
    
    public function getDataGraph3($start_date, $end_date){
        $query= new Query();
        $query->SELECT(["sum(case when unplanned='yes' then 1 else 0 end) as unplanned",
                                "sum(case when unplanned!='yes' or unplanned is null then 1 else 0 end) as planned",
                                "sum(no_of_pages) as pages_closed",
                                "WEEK(request_received_on) as week",
                                "request_received_on - INTERVAL WEEKDAY(request_received_on) Day as mondaythisweek",
                                'request_received_on'])
                    ->from('task')
                    ->where("close='yes'");
                    if(!empty($start_date)){
                        
                        $query->andWhere(['>=','request_received_on',date('Y-m-d',strtotime($start_date) )]);
                    }
                    if(!empty($end_date)){
                        $query->andWhere(['<=','request_received_on',date('Y-m-d',strtotime($end_date) )]);
                    }
                    $query->groupby("WEEK(request_received_on)");
                    $data=$query->all();
        return $data;
    }
    
    public function getDataGraph4(){
        $query= new Query();
        $data= $query->SELECT(["sum(case when unplanned='yes' then 1 else 0 end) as unplanned","sum(case when unplanned!='yes' or unplanned is null then 1 else 0 end) as planned","sum(no_of_pages) as pages_closed",'type_of_request'])
                    ->from('task')
                    ->where("close='yes'")
                    ->groupby('type_of_request')
                    ->all();
        return $data;
    }
    
    public function getDataGraph5(){
        $query= new Query();
        $data= $query->SELECT(["sum(case when unplanned='yes' then 1 else 0 end) as unplanned","sum(case when unplanned!='yes' or unplanned is null then 1 else 0 end) as planned","sum(no_of_pages) as pages_closed",'type_of_request'])
                    ->from('task')
                    ->where("close='yes'")
                    ->groupby('type_of_request')
                    ->all();
        return $data;
    }
        
    public function getDataGraph6($start_date, $end_date){
        $query= new Query();
        $data= $query->SELECT(["sum(case when no_of_pages <= 10 then 1 else 0 end) as small_task","sum(case when no_of_pages >= 10 and no_of_pages <=25 then 1 else 0 end) as medium_task", "sum(case when no_of_pages >= 25 then 1 else 0 end) as large_task"])
                    ->from('task');
                    if(!empty($start_date)){
                        
                        $query->Where(['>=','request_received_on',date('Y-m-d',strtotime($start_date) )]);
                    }
                    if(!empty($end_date)){
                        $query->andWhere(['<=','request_received_on',date('Y-m-d',strtotime($end_date) )]);
                    }
                     $data=$query->all();
        return $data;
    }
    
    public function getDataGraph7(){
        $query= new Query();
        $data= $query->SELECT(["sum(case when unplanned='yes' then 1 else 0 end) as unplanned","sum(case when unplanned!='yes' or unplanned is null then 1 else 0 end) as planned","sum(no_of_pages) as pages_closed",'type_of_request'])
                    ->from('task')
                    ->where("close='yes'")
                    ->groupby('type_of_request')
                    ->all();
        return $data;
    }
    
    public function getDataGraph8(){
        $query= new Query();
        $data= $query->SELECT(["sum(case when unplanned='yes' then 1 else 0 end) as unplanned","sum(case when unplanned!='yes' or unplanned is null then 1 else 0 end) as planned","sum(no_of_pages) as pages_closed",'type_of_request'])
                    ->from('task')
                    ->where("close='yes'")
                    ->groupby('type_of_request')
                    ->all();
        return $data;
    }
    
    public function getDataGraph9(){
        $query= new Query();
        $data= $query->SELECT(["sum(case when unplanned='yes' then 1 else 0 end) as unplanned","sum(case when unplanned!='yes' or unplanned is null then 1 else 0 end) as planned","sum(no_of_pages) as pages_closed",'type_of_request'])
                    ->from('task')
                    ->where("close='yes'")
                    ->groupby('type_of_request')
                    ->all();
        return $data;
    }
    
    public function getDataGraph10(){
        $query= new Query();
        $data= $query->SELECT(["sum(case when unplanned='yes' then 1 else 0 end) as unplanned","sum(case when unplanned!='yes' or unplanned is null then 1 else 0 end) as planned","sum(no_of_pages) as pages_closed",'type_of_request'])
                    ->from('task')
                    ->where("close='yes'")
                    ->groupby('type_of_request')
                    ->all();
        return $data;
    }
    
    public function getDataGraph11(){
        $query= new Query();
        $data= $query->SELECT(["sum(case when unplanned='yes' then 1 else 0 end) as unplanned","sum(case when unplanned!='yes' or unplanned is null then 1 else 0 end) as planned","sum(no_of_pages) as pages_closed",'type_of_request'])
                    ->from('task')
                    ->where("close='yes'")
                    ->groupby('type_of_request')
                    ->all();
        return $data;
    }
					
    public function rules(){
        return [
            [['case_id'], 'required'],
			 [['case_id'], 'unique'],
            [['case_id', 'practice_id', 'no_of_pages', 'revised_pages'], 'integer'],
            [['request_received_on', 'assigned_on', 'review_assigned_on', 'review_done_on', 'recode_sent_on', 'closed_date'], 'safe'],
            [['estimated_time', 'development_time', 'review_time'], 'number'],
            [['case_manager', 'assigned_to', 'note', 'reviewer', 'send_for_review', 'recode_sent_to'], 'string', 'max' => 50],
            [['review_done', 'send_for_recode', 'close', 'unplanned', 'sdlc_phase', 'file_type'], 'string', 'max' => 10],
            [['type_of_request'], 'string', 'max' => 25],
        ];
    }

    public function attributeLabels(){
        return [
            'task_id' => 'Task ID',
            'case_id' => 'Case ID',
            'practice_id' => 'Practice ID',
            'case_manager' => 'Case Manager',
            'request_received_on' => 'Request Received On',
            'no_of_pages' => 'No Of Pages',
            'revised_pages' => 'Revised Pages',
            'assigned_to' => 'Assigned To',
            'note' => 'Note',
            'assigned_on' => 'Assigned On',
            'reviewer' => 'Reviewer',
            'review_assigned_on' => 'Review Assigned On',
            'review_done' => 'Review Done',
            'review_done_on' => 'Review Done On',
            'send_for_review' => 'Send For Review',
            'recode_sent_to' => 'Recode Sent To',
            'recode_sent_on' => 'Recode Sent On',
            'send_for_recode' => 'Send For Recode',
            'close' => 'Close',
            'closed_date' => 'Closed Date',
            'unplanned' => 'Unplanned',
            'sdlc_phase' => 'Sdlc Phase',
            'type_of_request' => 'Type Of Request',
            'file_type' => 'File Type',
            'estimated_time' => 'Estimated Time',
            'development_time' => 'Development Time',
            'review_time' => 'Review Time',
        ];
    }
}
