<?php

namespace frontend\models;
use yii\db\Query;


use Yii;

/**
 * This is the model class for table "task".
 *
 * @property int $task_id
 * @property int $case_id
 * @property int $practice_id
 * @property string $case_manager
 * @property string $request_received_on
 * @property int $no_of_pages
 * @property int $revised_pages
 * @property string $assigned_to
 * @property string $note
 * @property string $assigned_on
 * @property string $reviewer
 * @property string $review_assigned_on
 * @property string $review_done
 * @property string $review_done_on
 * @property string $send_for_review
 * @property string $recode_sent_to
 * @property string $recode_sent_on
 * @property string $send_for_recode
 * @property string $close
 * @property string $closed_date
 * @property string $unplanned
 * @property string $sdlc_phase
 * @property string $type_of_request
 * @property string $file_type
 * @property string $estimated_time
 * @property string $development_time
 * @property string $review_time
 */
class Task extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task';
    }
	
	public function getRole(){
		$query= new Query();
		$data= $query->SELECT(['role'])
					->FROM('user')
					//->where (['id' => Yii::$app->user->identity->getId()])
					->all();
		$b =\yii\helpers\ArrayHelper::map($data, 'role', 'role');
		return $data;
	}
	
	public function getListAssignedTo(){
		//$test=['user1'=>'user1', 'user2'=>'user2'];
		//$connection= \Yii::$app->db;
		//$model = $connection->createCommand('SELECT username FROM user');
		//$users = $model->queryAll();
		$query= new Query();
		$data= $query->SELECT(['username'])
					->FROM('user')
					->all();
		$b= \yii\helpers\ArrayHelper::map($data, 'username', 'username');
		
		echo "<pre>";
		//print_r ($b);
		echo "</pre>";
		return $b;
	}
	public function getReviewer(){
		$query= new Query();
		$data= $query->SELECT(['username'])
					->FROM('user')
					->all();
		$b =\yii\helpers\ArrayHelper::map($data, 'username', 'username');
		return $b;
	}
	
	public function getFileType(){
		$query= new Query();
		$data= $query->SELECT(['file'])
					->FROM('file_type')
					->all();
		$b =\yii\helpers\ArrayHelper::map($data, 'file', 'file');
		return $b;
	}
	
	public function getAssigned(){
		$query= new Query();
		$data= $query->SELECT(['task_id'])
					->FROM('task')
					->all();
		$b =\yii\helpers\ArrayHelper::map($data, 'task_id', 'task_id');
		return $b;
	}
	
	public function getSDLCPhase(){
		$query= new Query();
		$data= $query->SELECT(['phase'])
					->FROM('sdlc_phase')
					->all();
		$b =\yii\helpers\ArrayHelper::map($data, 'phase', 'phase');
		return $b;
	}
	
	
	public function getDataGraph1(){
		$query= new Query();
		
		$data= $query->SELECT(['type_of_request',"count(task_id) as cases_closed","sum(no_of_pages) as pages_closed"])
					->from('task')
					->where("close='yes'")
					->groupby('type_of_request')
					->all();
		
		
		//$b =\yii\helpers\ArrayHelper::index($data, 'type_of_request');
		
		
		echo "<pre>";
		//print_r ($b);
		//print_r ($data);
		echo "</pre>";
		//exit();
		return $data;
	}
	public function getleaderboard(){
		$query= new Query();
		
		$data= $query->SELECT(['assigned_to','concat(firstname," ",lastname) as userfullname',"count(task_id) as count","sum(no_of_pages) as pages"])
					->from('task')
                                        ->leftJoin('user','user.username = task.assigned_to')    
					->where("close='no'")
					->groupby('assigned_to')
					->all();
		return $data;
	}				
	public function getCountTotalTask() 
        {
                $query = new Query();

                return $query->SELECT(['count(task_id) as total_task'])
                                ->from('task')
                                ->scalar();
        }
        
        public function getTotalCloseTask() 
        {
                $query = new Query();

                return $query->SELECT(['count(task_id) as close_task'])
                                ->from('task')
                                ->where("close='yes'")
                                ->scalar();
        }
        
         public function getTotalOpenTask() 
        {
                $query = new Query();

                return $query->SELECT(['count(task_id) as open_task'])
                                ->from('task')
                                ->where("close='no'")
                                ->scalar();
        }
         public function getTotalPagesCoded() 
        {
                $query = new Query();

                return $query->SELECT(['sum(no_of_pages) as pages_closed'])
                               ->from('task')
                                ->where("close='yes'")
                                ->scalar();
        }
    public function getTypeOfRequest(){
		$query= new Query();
		$data= $query->SELECT(['request'])
					->FROM('type_of_request')
					->all();
		$b =\yii\helpers\ArrayHelper::map($data, 'request', 'request');
		return $b;
	}
	
	//public function getInReview
	
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['case_id'], 'required'],
			 [['case_id'], 'unique'],
            [['case_id', 'practice_id', 'no_of_pages', 'revised_pages'], 'integer'],
            [['request_received_on', 'assigned_on', 'review_assigned_on', 'review_done_on', 'recode_sent_on', 'closed_date'], 'safe'],
            [['estimated_time', 'development_time', 'review_time'], 'number'],
            [['case_manager', 'assigned_to', 'note', 'reviewer', 'send_for_review', 'recode_sent_to'], 'string', 'max' => 50],
            [['review_done', 'send_for_recode', 'close', 'unplanned', 'sdlc_phase', 'file_type'], 'string', 'max' => 10],
            [['type_of_request'], 'string', 'max' => 25],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'task_id' => 'Task ID',
            'case_id' => 'Case ID',
            'practice_id' => 'Practice ID',
            'case_manager' => 'Case Manager',
            'request_received_on' => 'Request Received On',
            'no_of_pages' => 'No Of Pages',
            'revised_pages' => 'Revised Pages',
            'assigned_to' => 'Assigned To',
            'note' => 'Note',
            'assigned_on' => 'Assigned On',
            'reviewer' => 'Reviewer',
            'review_assigned_on' => 'Review Assigned On',
            'review_done' => 'Review Done',
            'review_done_on' => 'Review Done On',
            'send_for_review' => 'Send For Review',
            'recode_sent_to' => 'Recode Sent To',
            'recode_sent_on' => 'Recode Sent On',
            'send_for_recode' => 'Send For Recode',
            'close' => 'Close',
            'closed_date' => 'Closed Date',
            'unplanned' => 'Unplanned',
            'sdlc_phase' => 'Sdlc Phase',
            'type_of_request' => 'Type Of Request',
            'file_type' => 'File Type',
            'estimated_time' => 'Estimated Time',
            'development_time' => 'Development Time',
            'review_time' => 'Review Time',
        ];
    }
}
