-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 05, 2018 at 04:01 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yii2advanced`
--

-- --------------------------------------------------------

--
-- Table structure for table `task`
--

CREATE TABLE `task` (
  `task_id` int(11) NOT NULL,
  `case_id` int(11) NOT NULL,
  `practice_id` int(11) DEFAULT NULL,
  `case_manager` varchar(50) DEFAULT NULL,
  `request_received_on` date DEFAULT NULL,
  `no_of_pages` int(11) DEFAULT NULL,
  `revised_pages` int(11) DEFAULT NULL,
  `assigned_to` varchar(50) DEFAULT NULL,
  `note` varchar(50) DEFAULT NULL,
  `assigned_on` date DEFAULT NULL,
  `reviewer` varchar(50) DEFAULT NULL,
  `review_assigned_on` date DEFAULT NULL,
  `review_done` varchar(10) DEFAULT NULL,
  `review_done_on` date DEFAULT NULL,
  `send_for_review` varchar(50) DEFAULT NULL,
  `recode_sent_to` varchar(50) DEFAULT NULL,
  `recode_sent_on` date DEFAULT NULL,
  `send_for_recode` varchar(10) DEFAULT NULL,
  `close` varchar(10) DEFAULT NULL,
  `closed_date` date DEFAULT NULL,
  `unplanned` varchar(10) DEFAULT NULL,
  `sdlc_phase` varchar(10) DEFAULT NULL,
  `type_of_request` varchar(25) DEFAULT NULL,
  `file_type` varchar(10) DEFAULT NULL,
  `estimated_time` decimal(10,0) DEFAULT NULL,
  `development_time` decimal(10,0) DEFAULT NULL,
  `review_time` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `task`
--

INSERT INTO `task` (`task_id`, `case_id`, `practice_id`, `case_manager`, `request_received_on`, `no_of_pages`, `revised_pages`, `assigned_to`, `note`, `assigned_on`, `reviewer`, `review_assigned_on`, `review_done`, `review_done_on`, `send_for_review`, `recode_sent_to`, `recode_sent_on`, `send_for_recode`, `close`, `closed_date`, `unplanned`, `sdlc_phase`, `type_of_request`, `file_type`, `estimated_time`, `development_time`, `review_time`) VALUES
(2, 7548125, 525, 'test', '2018-05-08', 1, 5, '', 'testnote', '2018-05-08', 'test', '2018-05-08', 'yes', '2018-05-08', 'yes', '', '0000-00-00', 'prompt', 'yes', NULL, 'prompt', 'Review', 'collector tweak', '', NULL, NULL, NULL),
(11, 111, 1, 'a', NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'yes', NULL, NULL, NULL, 'collector code', NULL, NULL, NULL, NULL),
(12, 222, 2, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, 'collector tweak', NULL, NULL, NULL, NULL),
(13, 333, 3, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'yes', NULL, NULL, NULL, 'clinical code', NULL, NULL, NULL, NULL),
(14, 444, 4, NULL, NULL, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'yes', NULL, NULL, NULL, 'clinical tweak', NULL, NULL, NULL, NULL),
(16, 555, 5, NULL, NULL, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, 'clinical tweak', NULL, NULL, NULL, NULL),
(17, 666, 6, NULL, NULL, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'yes', NULL, NULL, NULL, 'clinical code', NULL, NULL, NULL, NULL),
(18, 777, 7, NULL, NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'yes', NULL, NULL, NULL, 'collector tweak', NULL, NULL, NULL, NULL),
(19, 888, 8, NULL, NULL, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'yes', NULL, NULL, NULL, 'collector code', NULL, NULL, NULL, NULL),
(20, 999, 9, NULL, NULL, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, 'clinical tweak', NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `task`
--
ALTER TABLE `task`
  ADD PRIMARY KEY (`task_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `task`
--
ALTER TABLE `task`
  MODIFY `task_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
