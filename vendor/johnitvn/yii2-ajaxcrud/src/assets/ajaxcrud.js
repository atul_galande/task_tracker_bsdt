/*!
 * Ajax Crud 
 * =================================
 * Use for johnitvn/yii2-ajaxcrud extension
 * @author John Martin john.itvn@gmail.com
 */
$(document).ready(function () {

    // Create instance of Modal Remote
    // This instance will be the controller of all business logic of modal
    // Backwards compatible lookup of old ajaxCrubModal ID
    if ($('#ajaxCrubModal').length > 0 && $('#ajaxCrudModal').length == 0) {
        modal = new ModalRemote('#ajaxCrubModal');
    } else {
        modal = new ModalRemote('#ajaxCrudModal');
    }

    // Catch click event on all buttons that want to open a modal
    $(document).on('click', '[role="modal-remote"]', function (event) {
        event.preventDefault();

        // Open modal
        modal.open(this, null);
    });

    // Catch click event on all buttons that want to open a modal
    // with bulk action
    $(document).on('click', '[role="modal-remote-bulk"]', function (event) {
        event.preventDefault();

        // Collect all selected ID's
        var selectedIds = [];
        $('input:checkbox[name="selection[]"]').each(function () {
            if (this.checked)
                selectedIds.push($(this).val());
        });

        if (selectedIds.length == 0) {

            var pathArray = window.location.pathname.split('/');

            // If no selected ID's show warning
            modal.show();

            if (pathArray[2] != undefined && pathArray[2] == 'assessment') {
                modal.setHeader('<button aria-hidden="true" data-dismiss="modal" class="close" type="button"></button>\n\
<h4 class="modal-title">No Item(s) Selected</h4>');
                modal.setTitle('No Item(s) Selected');
                modal.setContent('Please select assessment(s) to release.');
            } else {
                modal.setTitle('No selection');
                modal.setContent('You must select item(s) to use this action');
            }

            modal.addFooterButton("Close", 'button', 'btn btn-close', function (button, event) {
                this.hide();
            });
        } else {
            // Open modal
            modal.open(this, selectedIds);
        }
    });
});