-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 12, 2018 at 02:05 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yii2advanced`
--

-- --------------------------------------------------------

--
-- Table structure for table `file_type`
--

CREATE TABLE `file_type` (
  `id` int(15) NOT NULL,
  `file` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `file_type`
--

INSERT INTO `file_type` (`id`, `file`) VALUES
(1, 'PDF'),
(2, 'Word'),
(3, 'PNG'),
(4, 'Excel'),
(5, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1530011517),
('m130524_201442_init', 1530011519);

-- --------------------------------------------------------

--
-- Table structure for table `sdlc_phase`
--

CREATE TABLE `sdlc_phase` (
  `ID` int(11) NOT NULL,
  `phase` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sdlc_phase`
--

INSERT INTO `sdlc_phase` (`ID`, `phase`) VALUES
(1, 'Code'),
(2, 'Review'),
(3, 'Recode'),
(4, 'Resolved');

-- --------------------------------------------------------

--
-- Table structure for table `task`
--

CREATE TABLE `task` (
  `task_id` int(11) NOT NULL,
  `case_id` int(11) NOT NULL,
  `practice_id` int(11) DEFAULT NULL,
  `case_manager` varchar(50) DEFAULT NULL,
  `request_received_on` date DEFAULT NULL,
  `no_of_pages` int(11) DEFAULT NULL,
  `revised_pages` int(11) DEFAULT NULL,
  `assigned_to` varchar(50) DEFAULT NULL,
  `note` varchar(50) DEFAULT NULL,
  `assigned_on` date DEFAULT NULL,
  `reviewer` varchar(50) DEFAULT NULL,
  `review_assigned_on` date DEFAULT NULL,
  `review_done` varchar(10) DEFAULT NULL,
  `review_done_on` date DEFAULT NULL,
  `send_for_review` varchar(50) DEFAULT NULL,
  `recode_sent_to` varchar(50) DEFAULT NULL,
  `recode_sent_on` date DEFAULT NULL,
  `send_for_recode` varchar(10) DEFAULT NULL,
  `close` varchar(10) DEFAULT NULL,
  `closed_date` date DEFAULT NULL,
  `unplanned` varchar(10) DEFAULT NULL,
  `sdlc_phase` varchar(10) DEFAULT NULL,
  `type_of_request` varchar(25) DEFAULT NULL,
  `file_type` varchar(10) DEFAULT NULL,
  `estimated_time` decimal(10,0) DEFAULT NULL,
  `development_time` decimal(10,0) DEFAULT NULL,
  `review_time` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `task`
--

INSERT INTO `task` (`task_id`, `case_id`, `practice_id`, `case_manager`, `request_received_on`, `no_of_pages`, `revised_pages`, `assigned_to`, `note`, `assigned_on`, `reviewer`, `review_assigned_on`, `review_done`, `review_done_on`, `send_for_review`, `recode_sent_to`, `recode_sent_on`, `send_for_recode`, `close`, `closed_date`, `unplanned`, `sdlc_phase`, `type_of_request`, `file_type`, `estimated_time`, `development_time`, `review_time`) VALUES
(1, 856732, 132, 'Carol', '2018-06-25', 34, NULL, 'altafhusseind ', 'Software', '2018-07-04', 'sudhirbh    ', NULL, 'prompt', NULL, 'prompt', '', NULL, 'prompt', 'yes', NULL, 'yes', 'Resolved', 'Collector Code', 'PDF', '38', NULL, NULL),
(2, 856734, 135, 'John', '2018-07-06', 7, NULL, 'harshalf    ', 'Medical', '2018-07-08', 'samsong    ', NULL, 'yes', NULL, 'prompt', '', NULL, 'prompt', 'no', NULL, 'no', 'Recode', 'Clinical Code', 'Word', '16', NULL, NULL),
(3, 856739, 137, 'Mary', '2018-07-06', 32, NULL, 'dilp    ', 'App', '2018-07-12', 'shashankp    ', NULL, 'prompt', NULL, 'prompt', '', NULL, 'prompt', 'no', NULL, 'yes', 'Review', 'Collector Tweak', 'Excel', '40', NULL, NULL),
(4, 856738, 167, 'James', '2018-06-05', 12, NULL, 'nishithr    ', 'Software', '2018-06-08', 'shivshankarj    ', NULL, 'prompt', NULL, 'prompt', '', NULL, 'prompt', 'yes', NULL, 'yes', 'Resolved', 'Clinical Tweak', 'PNG', '20', NULL, NULL),
(5, 856735, 132, 'Carol', '2018-07-04', 12, 3, 'samsong    ', 'Software', '2018-07-12', 'atulga    ', '2018-08-16', 'yes', '2018-08-23', 'prompt', 'shitalbar    ', '2018-08-10', 'yes', 'yes', '2018-08-25', 'yes', 'Resolved', 'Collector Tweak', 'PNG', '18', '25', '15.00'),
(6, 856733, 145, 'Anthony', '2018-06-24', 37, NULL, 'atulga    ', 'Code', '2018-06-27', 'evonnelo    ', NULL, 'prompt', NULL, 'prompt', '', NULL, 'prompt', 'no', NULL, 'no', 'Recode', 'Clinical Tweak', 'PNG', '42', NULL, NULL),
(7, 856736, 120, 'Melinda', '2018-07-09', 67, NULL, 'evonnelo    ', 'Medicine', '2018-07-11', 'trikeshn    ', NULL, 'prompt', NULL, 'prompt', '', NULL, 'prompt', 'no', NULL, 'no', 'Recode', 'Collector Code', 'Other', '75', NULL, NULL),
(8, 856532, 125, 'Christo', '2018-04-30', 31, NULL, 'manasit    ', 'Medical', '2018-05-05', 'sandeepge    ', NULL, 'prompt', NULL, 'prompt', '', NULL, 'prompt', 'no', NULL, 'no', 'Review', 'Collector Tweak', 'Excel', '34', NULL, NULL),
(9, 854732, 198, 'Joseph', '2018-07-06', 9, NULL, 'rameshwarg', 'Code', '2018-07-09', 'vishalsin    ', NULL, 'prompt', NULL, 'prompt', '', NULL, 'prompt', 'yes', NULL, 'no', 'Resolved', 'Collector Code', 'PDF', '10', NULL, NULL),
(10, 876732, 138, 'Akash', '2018-06-28', 17, NULL, 'dilp    ', 'App', '2018-07-02', 'Admin', NULL, 'prompt', '2018-07-23', 'prompt', '', NULL, 'prompt', 'yes', '2018-07-24', 'no', 'Resolved', 'Clinical Code', 'Word', '20', NULL, NULL),
(11, 856432, 178, 'Shane', '2018-07-13', 178, NULL, 'shashankp    ', 'Code', '2018-07-20', 'harshalf    ', NULL, 'prompt', NULL, 'prompt', '', NULL, 'prompt', 'yes', NULL, 'yes', 'Resolved', 'Clinical Code', 'Word', '190', NULL, NULL),
(12, 866732, 157, 'Shawn', '2018-06-12', 40, NULL, 'shitalbar    ', 'Execute', '2018-06-13', 'manasit    ', NULL, 'prompt', NULL, 'prompt', '', NULL, 'prompt', 'yes', NULL, 'yes', 'Resolved', 'Clinical Tweak', 'PDF', '45', NULL, NULL),
(13, 853732, 123, 'Steve', '2018-06-21', 15, NULL, 'shivshankarj    ', 'Admin', '2018-06-29', 'nikitaja    ', NULL, 'prompt', NULL, 'prompt', '', NULL, 'prompt', 'no', NULL, 'yes', 'Code', 'Collector Tweak', 'Excel', '20', NULL, NULL),
(14, 856832, 124, 'Wilma', '2018-07-06', 90, NULL, 'santoshma    ', 'Work', '2018-07-09', 'rameshwarg', NULL, 'prompt', NULL, 'prompt', '', NULL, 'prompt', 'no', NULL, 'no', 'Code', 'Collector Code', 'PNG', '110', NULL, NULL),
(15, 816732, 136, 'Roque', '2018-07-19', 9, NULL, 'shitalbar    ', 'All the best', '2018-07-20', 'sandeepge    ', NULL, 'prompt', NULL, 'prompt', '', NULL, 'prompt', 'yes', NULL, 'no', 'Resolved', 'Clinical Code', 'Excel', '10', NULL, NULL),
(16, 856712, 140, 'Joycee', '2018-07-27', 23, NULL, 'samsong    ', 'Welcome', '2018-07-28', 'pratikshash    ', NULL, 'prompt', NULL, 'prompt', '', NULL, 'prompt', 'no', NULL, 'no', 'Code', 'Clinical Code', 'Word', '30', NULL, NULL),
(17, 857732, 134, 'Shanaya', '2018-05-24', 61, NULL, 'thoibisanad    ', 'Medical', '2018-06-06', 'nishithr    ', NULL, 'prompt', NULL, 'prompt', '', NULL, 'prompt', 'no', NULL, 'no', 'Recode', 'Clinical Tweak', 'Excel', '65', NULL, NULL),
(18, 814732, 174, 'Arun', '2018-07-13', 42, NULL, 'amolsak    ', 'Health', '2018-07-16', 'pratikshash    ', NULL, 'prompt', NULL, 'prompt', '', NULL, 'prompt', 'yes', NULL, 'yes', 'Resolved', 'Collector Tweak', 'Word', '45', NULL, NULL),
(19, 12, 124, 'test', '2018-07-12', 4, NULL, 'dilp    ', 'ere', '2018-07-12', 'nishithr    ', '2018-07-12', NULL, '2018-07-12', NULL, NULL, NULL, NULL, 'yes', '2018-07-12', 'no', 'Resolved', 'Collector Code', 'PDF', '20', '50', '30.00'),
(20, 232, 23, 'trt', '2018-07-12', 5, NULL, 'dilp    ', '', '2018-07-12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', 'Code', 'Collector Code', 'PDF', '60', NULL, NULL),
(21, 1234, 12, 'car', '2018-07-24', 32, NULL, 'harshalf    ', '', '2018-07-24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', 'Code', 'Collector Tweak', 'PNG', '16', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `type_of_request`
--

CREATE TABLE `type_of_request` (
  `ID` int(11) NOT NULL,
  `request` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `type_of_request`
--

INSERT INTO `type_of_request` (`ID`, `request`) VALUES
(1, 'Collector Code'),
(2, 'Collector Tweak'),
(3, 'Clinical Code'),
(4, 'Clinical Tweak');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) CHARACTER SET utf8 NOT NULL,
  `firstname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `role` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `firstname`, `lastname`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `role`, `created_at`, `updated_at`) VALUES
(1, 'Admin', NULL, NULL, 'LbF6Kgp2hkFvp_2MdU4NCILyNn_77hNI', '$2y$13$tmqa7pMxHveldFwIf1l.CuvsazNH3uZVEmWxvW90Emzs20mZTutIW', NULL, 'admindemo@cybage.com', 10, 'admin', 1530098920, 1530098920),
(2, 'test', NULL, NULL, 'LbF6Kgp2hkFvp_2MdU4NCILyNn_77hNI', '$2y$13$tmqa7pMxHveldFwIf1l.CuvsazNH3uZVEmWxvW90Emzs20mZTutIW', NULL, 'test@test.com', 10, 'dev', 1530098920, 1530098920),
(4, 'altafhusseind ', 'Altafhussein', 'Devjani', 'LbF6Kgp2hkFvp_2MdU4NCILyNn_77hNI', '$2y$13$tmqa7pMxHveldFwIf1l.CuvsazNH3uZVEmWxvW90Emzs20mZTutIW', NULL, 'altafhusseind@cybage.com', 10, 'admin', 1530098920, 1530098920),
(5, 'amolsak    ', 'Amol', 'Sakore', 'LbF6Kgp2hkFvp_2MdU4NCILyNn_77hNI', '$2y$13$tmqa7pMxHveldFwIf1l.CuvsazNH3uZVEmWxvW90Emzs20mZTutIW', NULL, 'amolsak@cybage.com', 10, 'dev', 1530098920, 1530098920),
(6, 'atulga    ', 'Atul', 'Galande', 'LbF6Kgp2hkFvp_2MdU4NCILyNn_77hNI', '$2y$13$tmqa7pMxHveldFwIf1l.CuvsazNH3uZVEmWxvW90Emzs20mZTutIW', NULL, 'atulga@cybage.com', 10, 'admin', 1530098920, 1530098920),
(7, 'dilp    ', 'Dil', 'Pradhan', 'LbF6Kgp2hkFvp_2MdU4NCILyNn_77hNI', '$2y$13$tmqa7pMxHveldFwIf1l.CuvsazNH3uZVEmWxvW90Emzs20mZTutIW', NULL, 'dilp@cybage.com', 10, 'dev', 1530098920, 1530098920),
(8, 'harshalf    ', 'Harshal', 'Funde', 'LbF6Kgp2hkFvp_2MdU4NCILyNn_77hNI', '$2y$13$tmqa7pMxHveldFwIf1l.CuvsazNH3uZVEmWxvW90Emzs20mZTutIW', NULL, 'harshalf@cybage.com', 10, 'dev', 1530098920, 1530098920),
(9, 'manasit    ', 'Manasi', 'Tikam', 'LbF6Kgp2hkFvp_2MdU4NCILyNn_77hNI', '$2y$13$tmqa7pMxHveldFwIf1l.CuvsazNH3uZVEmWxvW90Emzs20mZTutIW', NULL, 'manasit@cybage.com', 10, 'dev', 1530098920, 1530098920),
(10, 'nikitaja    ', 'Nikita', 'Jayawant', 'LbF6Kgp2hkFvp_2MdU4NCILyNn_77hNI', '$2y$13$tmqa7pMxHveldFwIf1l.CuvsazNH3uZVEmWxvW90Emzs20mZTutIW', NULL, 'nikitaja@cybage.com', 10, 'dev', 1530098920, 1530098920),
(11, 'nishithr    ', 'Nishith', 'Rawal', 'LbF6Kgp2hkFvp_2MdU4NCILyNn_77hNI', '$2y$13$tmqa7pMxHveldFwIf1l.CuvsazNH3uZVEmWxvW90Emzs20mZTutIW', NULL, 'nishithr@cybage.com', 10, 'admin', 1530098920, 1530098920),
(12, 'pratikshash    ', 'Pratiksha', 'Shirbhate', 'LbF6Kgp2hkFvp_2MdU4NCILyNn_77hNI', '$2y$13$tmqa7pMxHveldFwIf1l.CuvsazNH3uZVEmWxvW90Emzs20mZTutIW', NULL, 'pratikshash@cybage.com', 10, 'dev', 1530098920, 1530098920),
(13, 'rameshwarg', 'Rameshwar', 'Gupta', 'LbF6Kgp2hkFvp_2MdU4NCILyNn_77hNI', '$2y$13$tmqa7pMxHveldFwIf1l.CuvsazNH3uZVEmWxvW90Emzs20mZTutIW', NULL, 'rameshwarg@cybage.com', 10, 'dev', 1530098920, 1530098920),
(14, 'samsong    ', 'Samson', 'Ghagare', 'LbF6Kgp2hkFvp_2MdU4NCILyNn_77hNI', '$2y$13$tmqa7pMxHveldFwIf1l.CuvsazNH3uZVEmWxvW90Emzs20mZTutIW', NULL, 'samsong@cybage.com', 10, 'dev', 1530098920, 1530098920),
(15, 'santoshma    ', 'Santosh', 'Mane', 'LbF6Kgp2hkFvp_2MdU4NCILyNn_77hNI', '$2y$13$tmqa7pMxHveldFwIf1l.CuvsazNH3uZVEmWxvW90Emzs20mZTutIW', NULL, 'santoshma@cybage.com', 10, 'admin', 1530098920, 1530098920),
(16, 'shashankp    ', 'Shashank', 'Pawar', 'LbF6Kgp2hkFvp_2MdU4NCILyNn_77hNI', '$2y$13$tmqa7pMxHveldFwIf1l.CuvsazNH3uZVEmWxvW90Emzs20mZTutIW', NULL, 'shashankp@cybage.com', 10, 'dev', 1530098920, 1530098920),
(17, 'shitalbar    ', 'Shital', 'Bargal', 'LbF6Kgp2hkFvp_2MdU4NCILyNn_77hNI', '$2y$13$tmqa7pMxHveldFwIf1l.CuvsazNH3uZVEmWxvW90Emzs20mZTutIW', NULL, 'shitalbar@cybage.com', 10, 'dev', 1530098920, 1530098920),
(18, 'shivshankarj    ', 'Shivshankar', 'Johari', 'LbF6Kgp2hkFvp_2MdU4NCILyNn_77hNI', '$2y$13$tmqa7pMxHveldFwIf1l.CuvsazNH3uZVEmWxvW90Emzs20mZTutIW', NULL, 'shivshankarj@cybage.com', 10, 'admin', 1530098920, 1530098920),
(19, 'sudhirbh    ', 'Sudhir', 'Bharambe', 'LbF6Kgp2hkFvp_2MdU4NCILyNn_77hNI', '$2y$13$tmqa7pMxHveldFwIf1l.CuvsazNH3uZVEmWxvW90Emzs20mZTutIW', NULL, 'sudhirbh@cybage.com', 10, 'admin', 1530098920, 1530098920),
(20, 'sunitasi    ', 'Sunita', 'Singh', 'LbF6Kgp2hkFvp_2MdU4NCILyNn_77hNI', '$2y$13$tmqa7pMxHveldFwIf1l.CuvsazNH3uZVEmWxvW90Emzs20mZTutIW', NULL, 'sunitasi@cybage.com', 10, 'dev', 1530098920, 1530098920),
(21, 'thoibisanad    ', 'Thoibisana', 'Devi', 'LbF6Kgp2hkFvp_2MdU4NCILyNn_77hNI', '$2y$13$tmqa7pMxHveldFwIf1l.CuvsazNH3uZVEmWxvW90Emzs20mZTutIW', NULL, 'thoibisanad@cybage.com', 10, 'admin', 1530098920, 1530098920),
(22, 'trikeshn    ', 'Trikesh', 'Naidu', 'LbF6Kgp2hkFvp_2MdU4NCILyNn_77hNI', '$2y$13$tmqa7pMxHveldFwIf1l.CuvsazNH3uZVEmWxvW90Emzs20mZTutIW', NULL, 'trikeshn@cybage.com', 10, 'dev', 1530098920, 1530098920),
(23, 'vishalsin    ', 'Vishal', 'Singh', 'LbF6Kgp2hkFvp_2MdU4NCILyNn_77hNI', '$2y$13$tmqa7pMxHveldFwIf1l.CuvsazNH3uZVEmWxvW90Emzs20mZTutIW', NULL, 'vishalsin@cybage.com', 10, 'dev', 1530098920, 1530098920),
(24, 'evonnelo    ', 'Evonne', 'Lobo', 'LbF6Kgp2hkFvp_2MdU4NCILyNn_77hNI', '$2y$13$tmqa7pMxHveldFwIf1l.CuvsazNH3uZVEmWxvW90Emzs20mZTutIW', NULL, 'evonnelo@cybage.com', 10, 'admin', 1530098920, 1530098920),
(25, 'sandeepge    ', 'Sandeep', 'Gera', 'LbF6Kgp2hkFvp_2MdU4NCILyNn_77hNI', '$2y$13$tmqa7pMxHveldFwIf1l.CuvsazNH3uZVEmWxvW90Emzs20mZTutIW', NULL, 'sandeepge@cybage.com', 10, 'admin', 1530098920, 1530098920);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `file_type`
--
ALTER TABLE `file_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `sdlc_phase`
--
ALTER TABLE `sdlc_phase`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `task`
--
ALTER TABLE `task`
  ADD PRIMARY KEY (`task_id`);

--
-- Indexes for table `type_of_request`
--
ALTER TABLE `type_of_request`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `file_type`
--
ALTER TABLE `file_type`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `sdlc_phase`
--
ALTER TABLE `sdlc_phase`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `task`
--
ALTER TABLE `task`
  MODIFY `task_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `type_of_request`
--
ALTER TABLE `type_of_request`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
